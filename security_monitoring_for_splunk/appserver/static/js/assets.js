require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView) {
 // Load individual components
            var SearchManager = require("splunkjs/mvc/searchmanager");

            // Set up search managers
            var search1 = new SearchManager({
                id: "search1",
                search: "index=_internal | head 10000 | stats sparkline count by sourcetype | rangemap field=count low=0-100 elevated=101-1000 default=severe",
                earliest_time: "-1h@h", 
                latest_time: "now",
                preview: true,
                cache: true
            });

            var CustomTooltipRenderer = TableView.BaseCellRenderer.extend({
                        canRender: function(cell) {
                        return cell.field === 'src_ip';
                        },
        
                        render: function($td, cell) {
            
                                    var src_ip = cell.value;
                                    var tip = cell.value;
            
            //if(message.length > 48) { message = message.substring(0,47) + "..." }
            
                                    $td.html(_.template('<a href="#" data-toggle="tooltip" data-placement="left" title="<%- tip%>"><%- src_ip%></a>', {
                                    tip: tip,
                                    src_ip: src_ip 
                                    }));
            
            // This line wires up the Bootstrap tooltip to the cell markup
                        $td.children('[data-toggle="tooltip"]').tooltip();
                        }
            });
            
            $("#comms_network_traffic_detail_table").click(function(){
                console.log("cellData: ");
                         tableView.table.render();
            });
            });

});
