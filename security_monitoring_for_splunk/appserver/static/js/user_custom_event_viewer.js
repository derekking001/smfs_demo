require([
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/eventsviewerview',
    'splunkjs/mvc/searchmanager',
    'splunkjs/mvc',
    'underscore',
    'splunkjs/mvc/simplexml/ready!'],function(
    TableView,
    EventView,
    SearchManager,
    mvc,
    _
    ){
    var EventSearchBasedRowExpansionRenderer = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var destCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest';
            });
             var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            console.log("latest", latest);
            
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: '(eventtype="windows_system_access" NOT user="*$" (EventCode=4624 OR EventCode=4625 OR EventCode=528 OR EventCode=529))  dest=' + destCell.value + ' earliest=' + earliest + ' latest=' + latest + ' '});
            $container.append(this._EventView.render().el);
        }
    });
    
     var EventSearchBasedRowExpansionRenderer1 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search1',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search1',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var userCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'user';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="windows_security_authorisation_events" host=* (EventCode=4720 OR EventCode=4722 OR EventCode=4725 OR EventCode=4726 OR EventCode=4738 OR EventCode=4740 OR EventCode=4767 OR EventCode=624 OR EventCode=626 OR EventCode=629 OR EventCode=630 OR EventCode=642 OR EventCode=644 OR EventCode=671))  user=' + userCell.value });
            $container.append(this._EventView.render().el);
        }
    });
     
     
     
    var EventSearchBasedRowExpansionRenderer2 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search2',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search2',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var userCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'user';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="windows_security_group_change_events" host=* (EventCode=4723 OR EventCode=4724 OR EventCode=4727 OR EventCode=4728 OR EventCode=4729 OR EventCode=4730 OR EventCode=4731 OR EventCode=4732 OR EventCode=4733 OR EventCode=4734 OR EventCode=4735 OR EventCode=4737 OR EventCode=4754 OR EventCode=4755 OR EventCode=4756 OR EventCode=4757 OR EventCode=4758 OR EventCode=627 OR EventCode=628 OR EventCode=631 OR EventCode=632 OR EventCode=633 OR EventCode=634 OR EventCode=635 OR EventCode=636 OR EventCode=637 OR EventCode=638 OR EventCode=639 OR EventCode=641 OR EventCode=658 OR EventCode=659 OR EventCode=660 OR EventCode=661 OR EventCode=662))  user=' + userCell.value });
            $container.append(this._EventView.render().el);
        }
    });
    
     var EventSearchBasedRowExpansionRenderer3 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search3',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search3',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var destCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype=remote_vpn_events host=*)  dest=' + destCell.value });
            $container.append(this._EventView.render().el);
        }
    });  
    
      var EventSearchBasedRowExpansionRenderer4 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search4',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search4',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var userCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'user';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="windows_privileged_access" host=* NOT user="*$" (EventCode=4672 OR EventCode=576))   user=' + userCell.value });
            $container.append(this._EventView.render().el);
        }
    });  
    
    var tableElement1 = mvc.Components.getInstance("user_system_access_detail_table");
    tableElement1.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer());
    });
    
    var tableElement2 = mvc.Components.getInstance("user_authorisation_show_detail_table");
    tableElement2.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer1());
    });
    
    var tableElement3 = mvc.Components.getInstance("user_group_changes_detail_table");
    tableElement3.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer2());
    });
    
    var tableElement4 = mvc.Components.getInstance("user_vpn_access_detail_table");
    tableElement4.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer3());
    });
    
    var tableElement5 = mvc.Components.getInstance("user_priv_esc_detail_table");
    tableElement5.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer4());
    });
});