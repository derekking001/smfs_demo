require([
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/eventsviewerview',
    'splunkjs/mvc/searchmanager',
    'splunkjs/mvc',
    'underscore',
    'splunkjs/mvc/simplexml/ready!'],function(
    TableView,
    EventView,
    SearchManager,
    mvc,
    _
    ){
    var EventSearchBasedRowExpansionRenderer = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var recipientCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'recipient';
            });
             var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            console.log("latest", latest);
            
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: '(eventtype="email_events" host=*)  recipient=' + recipientCell.value + ' earliest=' + earliest + ' latest=' + latest + ' '});
            $container.append(this._EventView.render().el);
        }
    });
    
     var EventSearchBasedRowExpansionRenderer1 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search1',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search1',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var userCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'user';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="web_traffic_events" host=*)  user=' + userCell.value });
            $container.append(this._EventView.render().el);
        }
    });
     
     
     
    var EventSearchBasedRowExpansionRenderer2 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search2',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search2',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var srcIPCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'src_ip';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="dns_events" host=*)   src_ip=' + srcIPCell.value });
            $container.append(this._EventView.render().el);
        }
    });
    
     var EventSearchBasedRowExpansionRenderer3 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search3',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search3',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var destIPCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest_ip';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="dhcp_events" host=*)   dest_ip=' + destIPCell.value });
            $container.append(this._EventView.render().el);
        }
    });  
    
      var EventSearchBasedRowExpansionRenderer4 = TableView.BaseRowExpansionRenderer.extend({ 
        initialize: function(args) {
            this._searchManager = new SearchManager({
                id: 'expand_with_events_search4',
                preview: false
            });
            this._EventView = new EventView({
                managerid: 'expand_with_events_search4',
                type: 'raw'
            });
        },
        canRender: function(rowData) {
            return true;
        },
        render: function($container, rowData) {
            var destCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest';
            });
            var timeCell = _(rowData.cells).find(function (cell) {
               return cell.field === '_time';
            });
            earliest = Math.floor(new Date(timeCell.value)) / 1000;
            latest = Math.floor(new Date(timeCell.value)) / 1000 ;
            latest = latest + .001;
            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'earliest=' + earliest + ' latest=' + latest +' (eventtype="network_ids_events" host=*)   dest=' + destCell.value });
            $container.append(this._EventView.render().el);
        }
    });  
    
    var tableElement1 = mvc.Components.getInstance("comms_email_traffic_detail");
    tableElement1.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer());
    });
    
    var tableElement2 = mvc.Components.getInstance("comms_web_proxy_traffic_detail");
    tableElement2.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer1());
    });
    
    var tableElement3 = mvc.Components.getInstance("comms_dns_traffic_detail");
    tableElement3.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer2());
    });
    
    var tableElement4 = mvc.Components.getInstance("comms_dhcp_traffic_detail");
    tableElement4.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer3());
    });
    
    var tableElement5 = mvc.Components.getInstance("comms_ids_detail_table");
    tableElement5.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer4());
    });
});