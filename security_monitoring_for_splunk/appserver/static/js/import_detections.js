
var sseSearches = [];

$("#import-sse").click(function() {
    //console.log("Clicked")

    require([
    "jquery",
    "underscore"
    ], function($, _) {

      document.getElementById('import-sse').style.visible = false;


      $.ajax({
      url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook',
      type: 'GET',
      contentType: "application/json",
      async: true,
        
      success: function(returneddata) { 
        existingPlaybooks = returneddata; 
        //console.log('existingPlaybooks: ', existingPlaybooks);
        
        $.ajax( {
        url: '/static/app/Splunk_Security_Essentials/components/localization/ShowcaseInfo.json',
        type: 'GET',
        async: true,
  	    success: function(returneddata) {
          showCaseInfo = returneddata;
          // If Successful Read in All Searches from samples Searches
          //console.log(showCaseInfo);
          $.ajax( {
          url: '/static/app/Splunk_Security_Essentials/components/data/sampleSearches/showcase_simple_search.json',
          type: 'GET',
          async: true,
          success: function(returneddata) {
            xxsimpleSearches = returneddata;
            $.ajax( {
            url: '/static/app/Splunk_Security_Essentials/components/data/sampleSearches/showcase_standard_deviation.json',
            type: 'GET',
            async: true,
            success: function(returneddata) {
	    	  xx = returneddata;
          	  $.ajax( {
              url: '/static/app/Splunk_Security_Essentials/components/data/sampleSearches/showcase_first_seen_demo.json',
              type: 'GET',
              async: true,
              success: function(returneddata) {
	      	    yy = returneddata;
	      	    // Create One Big Object with all the sample searches read from the 3 files.
	            simpleSearches = Object.assign(xxsimpleSearches, xx, yy);	
	            //console.log(simpleSearches);
	            // Now start the matching process. Read ShowCase object, and look for actual Splunk Searches (Skipping Premium Searches)
	            
               
                for (key in showCaseInfo['summaries']) {
                    var record = {};	
                    if (showCaseInfo['summaries'][key].app === "Splunk_Security_Essentials" && showCaseInfo['summaries'][key].hasSearch === "Yes") {
                        
                        for (x=0; x < showCaseInfo['summaries'][key].examples.length; x++) {
                        	if (showCaseInfo['summaries'][key].examples[x].label === "Live Data") {
                        		record.link = showCaseInfo['summaries'][key].examples[x].name;
                                //console.log("Name: ", record.link);
                                //console.log("Woah:: ", simpleSearches[record.link]);
                                if (typeof simpleSearches[record.link] !== 'undefined') {
                                	record.splunk_search = simpleSearches[record.link].value;
                                	//console.log(showCaseInfo['summaries'][key]);  	
                    	            record.app = showCaseInfo['summaries'][key].app;
									record.description = showCaseInfo['summaries'][key].description;
									record.category = showCaseInfo['summaries'][key].category;
									record.mitreTechnique = showCaseInfo['summaries'][key].mitre_technique;
									record.mitreTactic = showCaseInfo['summaries'][key].mitre_tactic;
									record.mitre = showCaseInfo['summaries'][key].mitre;
									record.killchain = showCaseInfo['summaries'][key].killchain;
									record.name = showCaseInfo['summaries'][key].name;
									record.falsePositives = showCaseInfo['summaries'][key].knownFP;
									record.alertVolume = showCaseInfo['summaries'][key].alertvolume;
									record.howToImplement = showCaseInfo['summaries'][key].howToImplement;
									record.help = showCaseInfo['summaries'][key].help;
									record.operationalize = showCaseInfo['summaries'][key].operationalize;
									record.phantomPlaybooks = showCaseInfo['summaries'][key].phantomPlaybooks;
									record.relevance = showCaseInfo['summaries'][key].relevance;

								    for (ee = 0; ee < existingPlaybooks.length; ee++) {
								    	if (showCaseInfo['summaries'][key].name === existingPlaybooks[ee].name) {
								    		record.existsInKVStore = true;
								    	}
								    }

                                    sseSearches.push(record);                    	   
                                }
                        	}
                        }
    
                    }                	
                }
              
                // output to screen
                writeHost(sseSearches); 
    

            },
            error: function(err) {
           	    console.log('ERROR');
           	    err_elem = document.getElementById('error-handler');
       	        err_elem.innerHTML = "Error Fetching Security_Essentials Searches. Check it is installed";
       	        err_elem.classList.add('show');
            }
            });
    
         },
         error: function(err) {
             console.log('ERROR');
               err_elem.classList.add('show');           
             err_elem = document.getElementById('error-handler');
       	     err_elem.innerHTML = "Error Fetching Security_Essentials Searches. Check it is installed";
       	   
         }
         });
       },
       error: function(err) {
           console.log('ERROR');
           err_elem = document.getElementById('error-handler');
       	   err_elem.innerText = "Error Fetching Security_Essentials Searches. Check it is installed";
       	   err_elem.classList.add('show');
       }
       });
    },
    error: function(err) {
       	console.log('ERROR');
       	err_elem = document.getElementById('error-handler');
       	err_elem.innerText = "Error Fetching Security_Essentials Searches. Check it is installed";
       	err_elem.classList.add('show');
       	var btn = document.createElement('button');
       	btn.setAttribute('id', 'dismiss-error');
       	btn.classList.add('btn', 'btn-error');
       	btn.innerText = "Dismiss";
       	err_elem.appendChild(btn);
       	btn.addEventListener('click', function (event) {
       		window.location.href = "/app/security_monitoring_for_splunk/import?from=sse";
       	})
       
    }
    });
   
      },
    error: function(err) {
       	console.log('ERROR');
       	err_elem = document.getElementById('error-handler');
       	err_elem.innerText = "Error Fetching Current Playbook Searches.";
       	err_elem.classList.add('show');
       	var btn = document.createElement('button');
       	btn.setAttribute('id', 'dismiss-error');
       	btn.classList.add('btn', 'btn-error');
       	btn.innerText = "Dismiss";
       	err_elem.appendChild(btn);
       	btn.addEventListener('click', function (event) {
       		window.location.href = "/app/security_monitoring_for_splunk/import?from=sse";
       	})
       	
    }


  });
        
    
}) 


function writeHost(object) {

/*
<article class="card">
            <div class="card-header">
              <h4>Header Text</h4>
            </div>
            <div class="card-body">
              <p>Card Content</p>
            </div>
          </article>
*/
    
    for (i = 0; i < object.length; i++) {
        parentNode = document.getElementById("card-section");

        var card = document.createElement('article');
        card.setAttribute('class', 'card');
        
        // If Already imported as a playbook - set selected
        if (object[i].existsInKVStore) {
            card.setAttribute('selected', 'true');
            card.setAttribute('class', 'card imported');
        } else {
            card.setAttribute('class', 'card');
        }

        var cardHeader = document.createElement('div');
        cardHeader.setAttribute('class', 'card-header');

        
        var cardHeaderText = document.createElement('h2');
        cardHeaderText.textContent = object[i].name;

        cardHeader.appendChild(cardHeaderText);

        var hiddenElem = document.createElement('p');
        hiddenElem.setAttribute('style', 'visibility:hidden');
        hiddenElem.innerText = i;
        cardHeader.appendChild(hiddenElem);

        var cardBody = document.createElement('div');
        cardBody.setAttribute('class', 'card-body');

        var bodyText = document.createElement('p');
        
        var descText = object[i].description;

        // strip html tags
        var regex = /(<([^>]+)>)/ig;
        var body = object[i].description;
        descText = body.replace(regex, "");

        // test for alert volume in desc text 
        //let alertVolume = descText.match(/Alert\sVolume(\s)?:\s+[A-z\s]+/);
        if (object[i].alertVolume === null || object[i].alertVolume === 'undefined') {
            alertVolume = "Alert Volume: Unknown";
        } 
        
        let hasAlertText = descText.match(/Alert Volume/);
        if (hasAlertText) {
            descText = descText.substring(0, hasAlertText.index);

        }

        // set remaining desc to card body
        bodyText.textContent = descText;

        var alertDiv = document.createElement('div');
        alertDiv.setAttribute('class', 'alert-level');
        var alertSpan = document.createElement('span');
        alertSpan.setAttribute('id', 'alert-tag');
        alertSpan.innerHTML = "Alert Volume: " + object[i].alertVolume;

        alertDiv.appendChild(alertSpan);


        var footer = document.createElement('div');
        footer.setAttribute('class', 'footer');
        
        categories = object[i].category.split("|");
        for (x = 0; x < categories.length; x++) {
            var footerContent = document.createElement('span');
            footerContent.setAttribute('class', 'footer-tags');
            footerContent.innerHTML = categories[x];
            footer.appendChild(footerContent);
        };

        tick = document.createElement('span');
        tick.setAttribute('class', 'icon-check-circle');
        tick.setAttribute('style', 'visibility: hidden');
        
        cardBody.appendChild(bodyText);
        card.appendChild(cardHeader);
        card.appendChild(cardBody);
        card.appendChild(tick);
        card.appendChild(alertDiv);
        card.appendChild(footer);
        parentNode.appendChild(card);


    };
}


});

// Handle Card Clicks //
// ****************** //
var base = document.querySelector('#card-section'); // the container for the variable content
var selector = '.card'; // any css selector for children

base.addEventListener('click', function(event) {

  // find the closest parent of the event target that
  // matches the selector
  var closest = event.target.closest(selector);
  if (closest && base.contains(closest)) {
    // handle class event
    //console.log('clicked');
    //console.log(closest.getAttribute('selected'));
    
    // Skip if previously imported in this session
    if (closest.classList.contains('imported') !== 'true') {
   
        // Unselect a previously chosen search
        if (closest.getAttribute('selected') === 'true') {
            closest.removeAttribute('selected');
            closest.setAttribute('style', 'opacity: 1');


        } else {
            // Set Selected ang grab all the details needed to move into Playbooks
            closest.setAttribute('selected', 'true');
            closest.setAttribute('style', 'opacity: 0.3');
            //console.log(closest);

            // ************************************* //
            // Pop Modal and Save Details to KVStore //
            // ************************************* //
            popModal = grabPlaybookDetails(closest);
            //console.log('back from popped modal: ', popModal);


        }

    }
  }
});

function grabPlaybookDetails (inputInfo) {
    var elem = inputInfo;
    //console.log("ELEM: ", elem);

    require(["jquery",
            "underscore",
            "splunkjs/mvc",
            "splunkjs/mvc/textinputview",
            "splunkjs/mvc/utils",
            "/static/app/security_monitoring_for_splunk/js/Modal.js",
            "splunkjs/mvc/searchmanager",
            Splunk.util.make_full_url("/static/app/security_monitoring_for_splunk/lib/sendTelemetry.js"),  
        ],
        function($,
            _,
            mvc,
            TextInputView,
            utils,
            Modal,
            SearchManager,
            Telemetry

        ) {

          let elems = elem.children;
          let searchName = elems[0].innerText;
          let arrElem = elems[0].children[1].innerHTML;
          var savedOK = 0;
                 
          eventModal = new Modal('eventModal', {
            title: 'Configure Playbook: ' + searchName,
            destroyOnHide: true,
            type: 'wide'

          });

          $(eventModal.$el).on("hide", function() {
                    // Nothing at the moment
            try {
            if (saveResult == 1) {
                //console.log('Resetting Attribute: ', saveResult);
                elem.removeAttribute('selected');
                elem.setAttribute('style', 'opacity: 1');
            }

            }
            catch {

            }

                   
          });

          $(eventModal.$el).on("shown", function() {
            //console.log("Shown Triggered");
            $("#savedSearchModal #searchName").val($("#unique_id").val());
            $("#runSearch").click(function() {
            //console.log("Run Search Clicked");
            var searchVal = $("#eventModal #searchName").val();
            searchVal = encodeURIComponent(searchVal);
            var url = "/app/security_monitoring_for_splunk/search?q=search%20" + searchVal;
            //console.log("Trying to search: ", url);
            window.open(url, '_blank');

        });

          });

          let existsMessage = "";
          if (sseSearches[arrElem].existsInKVStore) {
          	existsMessage = "Already Exists in Playbooks. WARNING a duplicate will be created if you re-import!"
          }

          eventModal.body
                    .append($('<div id="playbook-wrapper">'+

                    '<div id="success-alert" class="alert alert-success alert-dismissable" hidden="true">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Success! Playbook Imported. </div>'+
                    '<div id="failure-alert" class="alert alert-danger alert-dismissable" hidden="true">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Error! Playbook Import Failed. </div>'+
                    '<div class="playbook-inputs-flex">'+
                    '<section id="already-imported"><p>' + existsMessage +'</p></section>'+
                    '</div>'+
                    '<div class="playbook-inputs-flex">'+
                    '<label class="playbook-flex-items" for="eventsource">Event Source : </label>'+
                    '<select class="playbook-flex-items input" id="eventsource" name="eventsource">'+
                    '<option value="dns">DNS</option><option value="dhcp">DHCP</option>'+
                    '<option value="email">Email</option><option value="endpoint_ids">Endpoint IDS</option>'+
                    '<option value="firewall">Firewall</option><option value="host_vulnerability">Host Vulnerability</option>'+
                    '<option value="malware">Malware</option><option value="network_ids">Network IDS</option>'+
                    '<option value="plug_and_play">Plug n Play</option><option value="processes">Processes</option>'+
                    '<option value="remote_vpn">Remote VPN</option><option value="web_traffic">Web Traffic</option>'+
                    '<option value="windows_security">Windows Security</option></select>'+
                    '<label class="playbook-flex-items" for="reportcategory">Report Category : </label>'+
                    '<select class="playbook-flex-items" id="reportcategory" name="reportcategory">'+
                    '<option value="APT">APT</option><option value="app_config">App Config</option>'+
                    '<option value="hot_threat">Hot Threat</option><option value="malware">Malware</option>'+
                    '<option value="policy">Policy</option><option value="playbook">Playbook</option>'+
                    '<option value="suspect_event">Suspect Event</option><option value="special_event">Special Event</option>'+
                    '<option value="trend">Trend</option><option value="target">Target</option></select></div>'+
                    '<textarea id="import-splunk-search-textarea" rows="5">' + sseSearches[arrElem].splunk_search +'</textarea>'+
                    '<section id="playbook-description"><h2>Description</h2>'+
                    '<p id="playbook-desc">'+ sseSearches[arrElem].description +'</p></section>'+
                    '<section id="playbook-implement"><h2>How to Implement</h2>'+
                    '<p id="how-to">' + sseSearches[arrElem].howToImplement + '</p></section></div>'+
                    '<div class="help-block"><p><i id="info" class="icon-info"></i>Operationalize in Playbook Editor</p></div>'));

                   
          eventModal.footer.append($('<button>').attr({
            type: 'button',
           'data-dismiss': 'modal'
            }).addClass('btn btn-default').text('Cancel').on('click', function() {
                  eventModal.hide();
                  
                  })
          );

          eventModal.footer.append($('<button>').attr({
            type: 'button',

            }).addClass('btn btn-primary').text('Import').on('click', function() {
                 //console.log("Import Button Pressed");

                 sseSearches[arrElem].report_category = $("#eventModal #reportcategory").val();
                 sseSearches[arrElem].event_source = $("#eventModal #eventsource").val();
                 sseSearches[arrElem].splunk_search = $("#eventModal #import-splunk-search-textarea").val();

                 let telemetryRec = {
                    "url_anon": window.location.href.replace(/http:\/\/.*?\//, "http://......../").replace(/https:\/\/.*?\//, "https://......../"),
                    "page": splunkjs.mvc.Components.getInstance("env").toJSON()['page'],
                    "splunk_version": splunkjs.mvc.Components.getInstance("env").toJSON()['version'],
                    "SSESearch" : sseSearches[arrElem].name

                 };
                 saveResult = postNewEntry(sseSearches[arrElem]);
                 //console.log('Back from Posting Entry');
                 if(saveResult === 1) {
                     $("#failure-alert").fadeTo(2000, 500).slideUp(500, function() {
                       $("#failure-alert").slideUp(500);
                       telemetryRec.importSSESuccess = false;
                      
                       return 1;
                        });
                 } else {
                        // Show success div/message.
                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                        $("#success-alert").slideUp(500);
                       //console.log("Elem is currently: ", elem);
                       elem.classList.add('imported');  
                       elems[0].innerText = elems[0].innerText + ' (Imported)'
                       telemetryRec.importSSESuccess = true;
                        });

                        setTimeout(function() {
                            $('#eventModal').modal('hide');

                            try {
                                Telemetry.SendTelemetryToSplunk("SaveSSESearch", telemetryRec);
                                
                            } 
                            catch(error) {
                            	// None
                            }
                         
                        },2000);

                 }
                 

              })
          );

          eventModal.show();
        // end of modalForSavedSearch function   

        
     });

}

// Write Security Essentials Data Away to KVStore //
// ********************************************** //

function postNewEntry(input) {
    //console.log('Input: ', input);
  
  var status = 0;
  var pageMode = 'new';
  let mitreTechnique = input.mitreTechnique;
  let mitreTactic = input.mitre;
  
  mitreTechnique = mitreTechnique.replace(/\|/g, ",");
  mitreTechnique = mitreTechnique.replace(/^,/, "");
  mitreTactic = mitreTactic.replace(/\|/g, ",")
  mitreTactic = mitreTactic.replace(/^,/, "");
  
  
  var record = {
    name: input.name,
    unique_id: $("#unique_id").val(),
    fidelity: 'INV',
    event_source: input.event_source,
    report_category: input.report_category,
    description: input.description,
    objective: input.relevance,
    results_analysis: input.howToImplement + ' ' + input.help + input.falsePositives,
    splunk_search: input.splunk_search,
    analysts_comments: input.operationalize,
    mitre_tactic: mitreTactic,
    mitre_technique: mitreTechnique,
    tags: input.killchain,
    phantom: input.phantomPlaybooks,
    user: Splunk.util.getConfigValue("USERNAME")
  };

  //console.log("Record:", record);
  
  if(pageMode == 'new') {
  
    var Items = [];
      $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num',
             async: false, success: function(returneddata) { Items = returneddata; } });
      
      var obj = Items.find(o => o.event_source === input.event_source);
      //console.log("OBJ:", obj);
      
      // Failure - Can't save
      if(obj === undefined) {
          status = 1;
          return status;
      }
      
    var newNum = Number(obj.last_num) + 1;
    obj.last_num = newNum;
      
    $.ajax({
          url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num/' +encodeURIComponent(obj._key),
          type: 'POST',
          contentType: "application/json",
          async: false,
          data: JSON.stringify(obj),
          success: function(returneddata) { newkey = returneddata; }
    });
  
    //console.log("record name:", record.name);
    // Create unique String
    //record.unique_id = newNum + '-' + record.fidelity + '-' + record.name + '-' + record.report_category;
    record.unique_id = newNum + '-' + record.fidelity + '-' + record.name;
    // Setup Defaults. 
    record.owner = "Security_Essentials";
    record.last_edited_by = record.user;
    record.rev_num = 1;
    record._time = new Date().getTime() / 1000;
    record.date_last_edited = 0;
  }
  
  // Editing an existing record
  if(record.rev_num != 1) {
    record.rev_num = Number($('#revNum').val()) + 1;
    record.owner = $("#owner").val();
    record.last_edited_by = record.user;
    record.date_last_edited = new Date().getTime() / 1000;
  }
  
  //console.log("Posting Record: ", record);
  
  $.ajax({
        url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook',
        type: 'POST',
        contentType: "application/json",
        async: false,
        data: JSON.stringify(record),
        success: function(returneddata) { 
            newkey = returneddata; 
            savedOK = 1;
            //console.log("Setting SavedOK: ", savedOK);
            return 0;
        }
    });

}
