




$("#refreshButton").html($("<button class=\"btn btn-primary\">Refresh Current KVStore</button>").click(function() {
    $("#results").html("<h4>Loading...</h4>");
    pullKVStoreStatus();
}));



function pullKVStoreStatus() {
    var results = $('<table class="table table-chrome"><thead><tr><th>Unique_id</th><th>Description</th><th>User</th><th>Report Category</th></tr></thead><tbody></tbody></table>');
    var Items = [];
    $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook', async: false, success: function(returneddata) { Items = returneddata; } });
    // console.log("Got these Items", Items)
    for (var i = 0; i < Items.length; i++) {
        results.find("tbody").append('<tr><td>' + Items[i].unique_id + '</td><td>' + Items[i].description + '</td><td>' + Items[i].user + '</td><td>' + Items[i].report_category + '</td></tr>');
    }
    $("#results").html(results);
}





$("#addButton").css("display", "inline").html($("<button class=\"btn btn-primary\">Add Item to KVStore</button>").click(function() {
    postNewEntry();
    generateRandomTextForInputs();
    pullKVStoreStatus();
}));



function postNewEntry() {
    var record = {
        _time: (new Date).getTime() / 1000,
        unique_id: $("#unique_id").val(),
        fidelity: $("#fidelity").val(),
        event_source: $("#eventsource").val(), 
        report_category: $("#reportcategory").val(),
        description: $("#description").val(),
        objective: $("#objective").val(),
        results_analysis: $("#results").val(),
        splunk_search: $("#splunksearch").val(),
        analysts_comments: $("#analystscomments").val(),
        phantom: $("#eventsource").val(),
        user: Splunk.util.getConfigValue("USERNAME")
    };

    $.ajax({
        url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook',
        type: 'POST',
        contentType: "application/json",
        async: false,
        data: JSON.stringify(record),
        success: function(returneddata) { newkey = returneddata; }
    });

}

