  // Step 3 - EventType Configuration  
   $('.event_types').on('click', function() {
    event_type_id=this.id;
    //console.log("ET clicked: ", event_type_id);
   
  
  require(["jquery",
        "underscore",
        "splunkjs/mvc",
        "splunkjs/mvc/textinputview",
        "splunkjs/mvc/utils",
        "/static/app/security_monitoring_for_splunk/js/Modal.js",
        "splunkjs/mvc/searchmanager",  
    ],
    function($,
        _,
        mvc,
        TextInputView,
        utils,
        Modal,
        SearchManager
      
    ) {
    
 
    
    // Start of Save Splunk Search
    modalForSaveEventType();
    
    
    // Functions  
    function modalForSaveEventType() {
      saveEventTypeModal = new Modal('saveEventTypeModal', {
        title: 'Save Event Type: ' + event_type_id,
        destroyOnHide: true,
        type: 'wide'
        
      });
      
      $(saveEventTypeModal.$el).on("hide", function() {
                // Nothing at the moment
      });
      
      $(saveEventTypeModal.$el).on("shown", function() {
        console.log("Shown Triggered");
        //$("#savedSearchModal #searchName").val($("#unique_id").val());
         $("#runSearch").click(function() {
        console.log("Run Search Clicked");
        var searchVal = $("#saveEventTypeModal #searchName").val();
        searchVal = encodeURIComponent(searchVal);
        var url = "/app/security_monitoring_for_splunk/search?q=search%20" + searchVal;
        console.log("Trying to search: ", url);
         window.open(url, '_blank');
    });
        
      });
      
      saveEventTypeModal.body
                .append($('<div class="container detect_search_container"><div class="container-fluid modalContainer" solid 1px; "><div id="searchsuccess-alert" class="alert alert-success alert-dismissable" style="background-color: #5cc05c;" hidden="true"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Success! Event Type Saved. </div><div id="searchfailure-alert" class="alert alert-danger alert-dismissable" style="background-color: #e66465;" hidden="true"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Error! Event Type NOT saved. </div><div class="row" style="min-width: 400px;margin-left: 10px;margin-top:40px;"><div id="aa" class="col detect_search_col1"><label for="nameofeventtype" style="line-height: 20px;margin-bottom: 5px;">Search String:</label></div><div class="col detect_search_col2"><input class="dsi" name="nameofsearch" type="text" id="searchName" size="355"></div><div><a id="runSearch" class="icon-search" href="#"> Run Search</a></div><div class="help-block detect_search_col2">e.g. (index=main sourcetype=access_combined) <a href="https://docs.splunk.com/Documentation/Splunk/latest/Data/Whysourcetypesmatter" class="help" target="_blank" title="Splunk help" aria-label="Learn more about sourcetypes">Learn More <i class="icon-external"></i></a></div></div></div>'));
     
     
    
     
      saveEventTypeModal.footer.append($('<button>').attr({
        type: 'button',
       'data-dismiss': 'modal'
        }).addClass('btn btn-default').text('Cancel').on('click', function() {
              saveEventTypeModal.hide();
              })
      );
      
      saveEventTypeModal.footer.append($('<button>').attr({
        type: 'button',
        
        }).addClass('btn btn-primary').text('Save').on('click', function() {
               console.log("Save Button Pressed");
                saveEventType();
              return true;
                 
               })
      );
      
      saveEventTypeModal.show();
    // end of modalForSavedSearch function   
         
    }
    
    
    function saveEventType() {
      //var vals = values;
      //console.log("Values: ", vals)
      var CheckStanzaID = Math.random().toString(36).replace(/[^a-z]+/g, '');
      //if(typeof splunkjs.mvc.Components.getInstance(CheckStanza) === "object") {
      //      console.log("Still have object set");
      //      splunkjs.mvc.Components.revokeInstance(CheckStanza);
     // }
      
      //if(typeof CheckStanza === "object") {
      //  console.log("xxx Still have object set");
      //      splunkjs.mvc.Components.revokeInstance(CheckStanza);
      //}
      
      CheckStanza = new SearchManager({
         "id": CheckStanzaID,
         "cancelOnUnload": true,
         "latest_time": "",
         "status_buckets": 0,
         "earliest_time": "0",
         "app": utils.getCurrentApp(),
         "auto_cancel": 90,
         "preview": true,
         "runWhenTimeIsUndefined": false,
         "autostart": false
         }, { tokens: true, tokenNamespace: "submitted" });
      
      CheckStanza = CheckStanza.data('results', { output_mode: 'json', count: 0 });
      
      // Setup the search string
      //var uniqueSavedSearch = "9999999";
      var searchString = "| rest  /servicesNS/-/security_monitoring_for_splunk/saved/eventtypes/ | table title description | search title=\"" + event_type_id + "\" | stats count";
      theSearch = mvc.Components.getInstance(CheckStanzaID);
      theSearch.set("search", searchString);
      theSearch.startSearch();
      console.log("Starting search");
      
      CheckStanza.on('search:error', function(properties) {
        console.log("Error Running Search", properties);
        return false;
      });
      
      CheckStanza.on('search:fail', function(properties) {
        console.log("Search Failed to start", properties);
        return false;
      });
      
      CheckStanza.on('search:done', function(properties) {
        console.log("Triggered: ", CheckStanza.attributes.data.resultCount);
        if (CheckStanza.attributes.data.resultCount == 0) {
          console.log("Search Does not exist already - creating new stanza");
          return false;
        } else {
          console.log("Search DOES exist - Need to UPDATE this one.");
          return false;
        }
        
      });
      
      CheckStanza.on("data", function(properties) {
        var data = CheckStanza.data().results;
        // console.log("Got my results", data)
        if (data[0].count == 0) {
             console.log("EventType Does not exist already - Something went wrong.");
              //CreateSavedSearchConfig("mytestsearch", successCallback, failureCallback);
              return false;
              
        } else {
            console.log("EventType DOES exist - Need to UPDATE this one.");
            var appscope = {};
            appscope.owner = Splunk.util.getConfigValue("USERNAME");
            appscope.app = utils.getCurrentApp();
            appscope.sharing = "app";    
            var mystanza = [];
            mystanza['search'] = $("#saveEventTypeModal #searchName").val();
            //mystanza['iseval'] = 0
            var svc = mvc.createService();
            var myEventTypes = svc.configurations(appscope);
            var fileDeferred = $.Deferred();
            myEventTypes.fetch({ 'search': 'name=eventtypes'}, function(err, myEventTypes) {
              var eventtypeFile = myEventTypes.item("eventtypes");
              if(!eventtypeFile) {
                // Create file here
                eventtypeFile.create('eventtypes', function(err, eventtypeFile) {
                  if(err) {
                    failureCallback();
                    return false;
                  }
                  fileDeferred.resolve(eventtypeFile);
                });
              
              } else {
                fileDeferred.resolve(eventtypeFile);
              }
              
            });
            fileDeferred.done(function(eventtypeFile) {
              eventtypeFile.post(event_type_id,mystanza, function(err,mystanza) {
                if (err) {
                  console.log("Caught Error", mystanza);
                  $("#searchfailure-alert").fadeTo(2000, 500).slideUp(500, function() {
                  $("#searchfailure-alert").slideUp(500);
                    });
                  setTimeout(function() {
                    $('#saveEventTypeModal').modal('hide');
                    },2000);
                  return false;
                } else {
                  console.log("Caught Success", err);
                   $("#searchsuccess-alert").fadeTo(2000, 500).slideUp(500, function() {
                   $("#searchsuccess-alert").slideUp(500);
                    });
                   
                   setTimeout(function() {
                    $('#saveEventTypeModal').modal('hide');
                    },2000);
                   
                  return true;
                }
              });
            });
              
            
            
            
        }
        
        console.log("Finished");
      });
      
     //endofsavedetectionsearch 
    }
    
     });
   });
    