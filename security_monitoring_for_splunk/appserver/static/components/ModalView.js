define([
        'underscore',
        'backbone',
        'jquery',
        'splunkjs/mvc',
        'splunkjs/mvc/searchmanager',
        'splunkjs/mvc/simplexml/element/table'
       ], function(_, Backbone, $, mvc, SearchManager, TableElement) {
                    
var modalTemplate = "<div class=\"modal fade\" id=\"comms_modal\" style=\"display: none;\" aria-hidden=\"true\">" +
                    "<div class=\"modal-dialog\">" +
                    "<div class=\"modal-content\">" +
                    "<div class=\"modal-header\"><h3><%- title %></h3></div>" +
                    "<div class=\"modal-body\"></div>" +
                    "<div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button></div>" +
                    "</div></div></div>";
                    
   
   var ModalView = Backbone.View.extend({
        defaults: {
            title: 'No title set'
        },
        initialize: function(options) {
            this.childViews = [];
            this.options = options;
            this.options = _.extend({},this.defaults, this.options);
            //console.log('Hello from Modal View: ' + this.options.title);
            this.template = _.template(modalTemplate);
        },
        events: {
            'click .close' : 'close',
            'click .modal-backdrop' : 'close'
        },
        render: function() {
            //console.log("In render Function");
            var data = { title : this.options.title };
            this.$el.html(this.template(data));
            return this;
        },
        show: function() {
            $(document.body).append(this.render().el);
             $('#comms_modal').modal('show');
            var template;
            if (this.options.nt_host) {
            template = "<html><table width=\"100%\"><tr><td align=\"right\">Manager: </td><td>" + this.options.manager + "</td></tr>" +
                "<tr><td align=\"right\">NT Hostname: </td><td>" + this.options.nt_host + "</td></tr>" +
                "<tr><td align=\"right\">Source IP: </td><td>" + this.options.src_ip + "</td></tr>" +
                "</table></html>";
            } else {
                template = "<html><table><tr><td>No Asset Information Found, Please update assets.csv lookup file.</td></tr></table></html>";
            }
            
            $(this.el).find('.modal-body').append("<div id=\"modalVizualization\">" + template + "</div>");
            
            //$(this.el).find('.modal').css({ 'width':'90%', 'height': 'auto', 'left': '5%', 'margin-left': '0', 'max-height': '100%' });
            
            //var search = mvc.Components.get(this.options.search.id);
            //console.log("Search", search);
            //var assetTable = new TableElement({
            //    id: assetTable,
            //    managerid: search.name,
            //    pageSize: "10",
            //    el: $('modalVizualization')
            //}).render();
            //this.childViews.push(assetTable);
            //search.startSearch();
            console.log(this.options.manager);
            
            
            
        },
        close: function() {
            this.unbind();
            this.remove();
            _.each(this.childViews, function(childView) {
                childView.unbind();
                childView.remove();
            });
            
        }
   });
    return ModalView;
    
});
   
