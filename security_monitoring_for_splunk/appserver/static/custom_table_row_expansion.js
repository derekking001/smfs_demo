require([
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/chartview',
    'splunkjs/mvc/searchmanager',
    'splunkjs/mvc',
    'underscore',
    'splunkjs/mvc/simplexml/ready!'],function(
    TableView,
    ChartView,
    SearchManager,
    mvc,
    _
    ){

    var EventSearchBasedRowExpansionRenderer = TableView.BaseRowExpansionRenderer.extend({
        initialize: function(args) {
            // initialize will run once, so we will set up a search and a chart to be reused.
            var tokens = mvc.Components.getInstance('submitted');
            this._searchManager = new SearchManager({
                id: 'details-search-manager',
                earliest_time: mvc.tokenSafe("$tok_time.earliest$"),
                latest_time: mvc.tokenSafe("$tok_time.latest$"),
                preview: false
            }, {tokens:true, tokenNamespace: "submitted"});
            
            this._chartView = new ChartView({
                managerid: 'details-search-manager',
                'charting.legend.placement': 'none'
            });
        },

        canRender: function(rowData) {
            // Since more than one row expansion renderer can be registered we let each decide if they can handle that
            // data
            // Here we will always handle it.
            return true;
        },

        render: function($container, rowData) {
            // rowData contains information about the row that is expanded.  We can see the cells, fields, and values
            // We will find the sourcetype cell to use its value
            var src_ipCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'src_ip';
            });
            var dest_ipCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest_ip';
            });
            var dest_portCell = _(rowData.cells).find(function (cell) {
               return cell.field === 'dest_port';
            });

            //update the search with the sourcetype that we are interested in
            this._searchManager.set({ search: 'eventtype=firewall_events src_ip=' + src_ipCell.value + ' dest_ip=' + dest_ipCell.value + ' dest_port='+ dest_portCell.value + ' | timechart sum(bytes) AS BytesTransferred'});

            // $container is the jquery object where we can put out content.
            // In this case we will render our chart and add it to the $container
            $container.append(this._chartView.render().el);
        }
    });


    var tableElement = mvc.Components.getInstance("comms_network_traffic_detail_table");
    tableElement.getVisualization(function(tableView) {
        // Add custom cell renderer, the table will re-render automatically.
        tableView.addRowExpansionRenderer(new EventSearchBasedRowExpansionRenderer());
    });


});
