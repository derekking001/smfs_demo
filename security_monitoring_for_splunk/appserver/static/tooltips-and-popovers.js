require(["jquery",
        "splunkjs/ready!",
        "bootstrap.popover",
        "bootstrap.tooltip"
    ],
    function(
        $, Ready) {
        
        $("[data-toggle=popover]").popover({
        html : true, 
        content: function() {
          var content = $(this).attr("data-popover-content");
          return $(content).children(".popover-body").html();
        },
        title: function() {
          return $("#example-popover-title").html();
        }
        
        
         
    });
        $("[data-toggle=tooltip]").tooltip();
        $("[data-toggle=Smallpopover]").popover();
    });
       
