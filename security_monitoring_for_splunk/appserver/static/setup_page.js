$(document).ready(function () {

   document.getElementById("step1Button").disabled = true;
  
    stepWizard();
   
   // Step 1 - Dependancy Checks
   $("#dependscheck").click(function() {
        document.getElementById("step1Button").disabled = false;       
        checkDependancies();
       
   });
   
   // Step 2 Technology Add-on Links
   // Grab Inputs when user changes - and suggest the splunkbase download link.
   $("#taWindowsInput").change(function() { getSplunkbaseLink("windows"); });
   $("#taFirewallInput").change(function() { getSplunkbaseLink("firewall"); });
   $("#taWebProxyInput").change(function() { getSplunkbaseLink("webProxy"); });
   $("#taEmailProxyInput").change(function() { getSplunkbaseLink("emailProxy"); });
   $("#taDHCPInput").change(function() { getSplunkbaseLink("dhcp"); });
   $("#taDNSInput").change(function() { getSplunkbaseLink("dns"); });
   $("#taNetworkIDSInput").change(function() { getSplunkbaseLink("networkIDS"); });
   $("#taVPNInput").change(function() { getSplunkbaseLink("vpn"); });
   $("#taMalwareInput").change(function() { getSplunkbaseLink("malware"); });
   $("#taProcessExecutionInput").change(function() { getSplunkbaseLink("processExecution"); });
   $("#taVulnerabilitiesInput").change(function() { getSplunkbaseLink("vulnerability"); });
   $("#taHostIDSInput").change(function() { getSplunkbaseLink("hostIDS"); });
   
   
   // Step 3 - EventType Configuration
   //$('.event_types').on('click', function() {
   //   console.log("clicked");                     
   //});
   
   
    
    
    // Step 4 - Field Extraction Checks
   $("#startValidationSearch").click(function() {
        validationSearches(); 
   });
   
   // Step 5 - Asset Configuration
   $("#dhcpAddRow").click(function() {
      assetsAddRow("dhcpWrapper");
   });
   $("#dnsAddRow").click(function() {
      assetsAddRow("dnsWrapper");
   });
   $("#emailAddRow").click(function() {
      assetsAddRow("emailWrapper");
   });
   $("#firewallAddRow").click(function() {
      assetsAddRow("firewallWrapper");
   });
   $("#malwareAddRow").click(function() {
      assetsAddRow("malwareWrapper");
   });
   $("#remotevpnAddRow").click(function() {
      assetsAddRow("remotevpnWrapper");
   });
   $("#webproxyAddRow").click(function() {
      assetsAddRow("webproxyWrapper");
   });
   $("#assetsEdit").click(function() {
      document.getElementById("assetsEdit").style.display = "none";
      document.getElementById("assetsSave").style.display = "block";
      
      document.getElementById("step5Button").disabled = true;
      
      
      var inputs = document.getElementById("inputsWrapper").getElementsByTagName('input');
      
      for(i = 0; i < inputs.length; i++) {
        var  elem = inputs[i];
        attr = elem.getAttribute("readOnly");
        
        if(attr) {
          console.log("Removing attribute readOnly");
          elem.removeAttribute("readOnly");
        }
      }
      
      //Toggle buttons 
      var buttons = document.getElementById("inputsWrapper").getElementsByClassName("btn pill");
      console.log("buttons", buttons);
      for(i = 0; i < buttons.length; i++) {
        buttons[i].removeAttribute("disabled");
      }
      
      
    // end of asset edits.  
   });
   
   $("#assetsSave").click(function() {
    
    // Lock inputs
    var inputs = document.getElementById("inputsWrapper").getElementsByTagName('input');  
    for(i = 0; i < inputs.length; i++) {
       var  elem = inputs[i];
      elem.setAttribute("readOnly", true);
        
    } 
    //Toggle buttons 
    var buttons = document.getElementById("inputsWrapper").getElementsByClassName("btn pill");
    for(i = 0; i < buttons.length; i++) {
        buttons[i].setAttribute("disabled", true);
    }
    
    if(!saveAssets()) {
    
      $("#failure-alert").fadeTo(2000, 500).slideUp(500, function() {
          $("#failure-alert").slideUp(500);
          return;
          });
      } else {
            document.getElementById("assetsEdit").style.display = "block";
            document.getElementById("assetsSave").style.display = "none";
            // Show success div/message.
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
            $("#success-alert").slideUp(500);
            document.getElementById("step5Button").disabled = false;
          });
    }
    //end of save asset function
   });
    
    
    // Step 6
    $("#searchRest").click(function() {
      
      $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/saved/searches?count=0&output_mode=json',
         async: false,
         success: function(returneddata) {
            data = returneddata;  } });
      
       $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/field_status',
         async: false,
         success: function(returneddata) {
            field_status = returneddata;  } });
        
         var row = 1;
         var searches = {};
         htmlResults = $('<h2 class="splunk-green">Saved Searches</h2>');
         htmlResults.append('<a id="enableButton" type="submit" "class=btn pill" name"enableButton" style="margin-left: 30px; margin-right:20px;">Enable/Disable Searches</a');
         //htmlResults.append('<a id="disableButton" type="button" "class=btn pill" name="disableButton">Disable</a');
         htmlResults.append('<table id="searchTable" class="table table-striped table-hover"><thead><tr><th>Select</th><th>Name</th><th>State</th><th>Schedule</th><th>Next Run</th><th>Has Req Data</th></thead><tbody style="color:white;"></tbody></table>');
            for (var i = 0; i < data.entry.length; i++) {
               
              if(data.entry[i].acl.app != "security_monitoring_for_splunk") {
                delete data.entry[i];
                continue;
              }
              
              if(! data.entry[i].name.match(/\d+/)) {
                delete data.entry[i];
                continue;
              }
      
              has_data_icon = "<a href=\"#\" title=\"Saved search name is not configured as a playbook, so i can't tell. Configure search as a playbook and rename the search to match the unique id provided.  " +
                              "\" data-placement=\"top\" data-content=\"No Content\" " +
                              "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-question-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
              
              // Look for searches matching Playbook naming scheme
              regex = /\d+-\w+-(\w+)+/;
              event_source = data.entry[i].name.match(regex);
              if(event_source != null) {
               for(j = 0; j < field_status.length; j++) {
                console.log("field_status:",field_status[j]);
                console.log("event_source:", event_source[1]);
                
                if(field_status[j].event_type.toString().toLowerCase() == event_source[1].toString().toLowerCase()) {
                  has_data = field_status[j].has_events;
                  if(has_data == 0) {
                    has_data_icon = "<a href=\"#\" title=\"I don't know " +
                                  ".\" data-placement=\"top\" data-content=\"No Content\" " +
                                  "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-x-circle\" style=\"color: bg-splunk-red; text-align: center; font-size: 25px;\" /></a>";
                  } else {
                    has_data_icon = "<a href=\"#\" title=\"Data Exists - OK" +
                                ".\" data-placement=\"top\" data-content=\"No Content\" " +
                                "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-check-circle\" style=\"color: #5cc05c; text-align: center; font-size: 25px;\" /></a>";
                  }
                  has_data = 0;   
                  
                }
                
               }
               
               
              }
              
              //console.log("eventSource:", event_source);                                          
              var state = (data.entry[i].content.disabled == false) ? "Enabled":"Disabled";
              htmlResults.find("tbody").append("<tr><td><input type=\"checkbox\" name=\"row" + row + "\" id=\"check" + row + "\" value=\"Enable\" " + 
                                  "</td><td> " + data.entry[i].name +
                                  "</td><td> " + state  +
                                  "</td><td> " + data.entry[i].content.cron_schedule +
                                  "</td><td>"  + data.entry[i].content.next_scheduled_time +
                                  "</td><td>"  + has_data_icon +
                                  "</td></tr>" );
              
              searches[row] = data.entry[i];
              row++;
            }
         
         $("#detectionSearchTable").html(htmlResults);
        
      
      $("#enableButton").click(function() {
        
        var tableContent = document.getElementById("searchTable");
        var rowLength = tableContent.rows.length;
        
          //require(['jquery',"splunkjs/mvc/utils","splunkjs/mvc/searchmanager"], function($, utils,SearchManager) {
          
              
            //if(typeof splunkjs.mvc.Components.getInstance(updateSearches) == "object") {
            //        splunkjs.mvc.Components.revokeInstance(updateSearches);
            // }
        
        
        
            //var updateSearches = new SearchManager ({
            //"id": "updateSearches",
            //"cancelOnUnload": true,
            //"latest_time": "",
            //"status_buckets": 0,
            //"earliest_time": "0",
            //"app": utils.getCurrentApp(),
            //"preview": true,
            //"runWhenTimeIsUndefined": false,
            //"autostart": false  
        //},
        //{ tokens: true, tokenNamespace: "submitted"  });
                var desiredState;
                for(row = 1; row < rowLength; row++) {
                    var cells = tableContent.rows.item(row).cells;
                    var checkbox = "check"+row;
                    var action = document.getElementById(checkbox).checked;
                    var searchName = cells.item(1).innerText;
                    var currentState = cells.item(2).innerText;
                    console.log("Looping the table");
                    
          
                    // TODO - ONLY ENABLE IF DATA ALSO MARKED AS AVAILABLE....
                    if(action) {
                      console.log("Found something to do");
                        var str = searchName + "";
                        str = encodeURIComponent(str);
                        if(currentState == 'Enabled') {
                           str = str + '?disabled=1';
                           desiredState = 'Disabled';
                        } else {
                           str = str + '?disabled=0';
                           desiredState = 'Enabled';
                        }
                        console.log("Trying AJAX with :", str);
                        $.ajax({
                           url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/saved/searches/' + str,
                             type: 'POST',
                             contentType: "application/json",
                             async: false,
                          success: function(returneddata) {
                              newkey = returneddata;
                              console.log("New Key:", newkey);
                              cells.item(2).innerText = desiredState;
                              document.getElementById(checkbox).checked = false;
                           }
                          });
          
                    
                    // end of enable condition
                    }

                }     
         // End of Require 
         });
      // End of Enable Function  
      });
      
      $("#disableButton").click(function() {
        console.log("Disable Clicked");
      });
  
     // End of Step 6 wizard Code 
    });
// End of Document Ready    
//});

function saveAssets() {
  
  items = document.getElementById("inputsWrapper").getElementsByTagName("input");
  infrastructure = [];
  // Save Records.
  for(i = 0; i < items.length; i = i + 4) {
    dvc_type = items[i].className.substr(0, items[i].className.search("InputClass"));
  
    //TODO INPUT VALIDATION
    
    // Dont write empty records.  
    if(items[i].value == "" && items[i+1].value == "" && items[i+2].value == "" ) {
      console.log("Found NULL");
      continue; 
    }
  
    record = {dvc_type: dvc_type, ip: items[i].value, nt_host: items[i+1].value, dns: items[i+2].value, key: items[i+3].value };
    
    // If already in the KVStore add the key to URL 
    if(record.key === undefined || record.key == "" ) {
      keyId = '';
    } else {
      keyId = '/'+record.key;
    }
    
    $.ajax({
        url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/infrastructure' + keyId,
        type: 'POST',
        contentType: "application/json",
        async: false,
        data: JSON.stringify(record),
        success: function(returneddata) { newkey = returneddata; }
    });
    
    if(newkey === undefined || newkey == "") {
      return false;
    }
    
  }
  
  return true;
}

function loadAssetPage() {
  $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/infrastructure',
         async: false,
         success: function(returneddata)
         { Items = returneddata; } });
           
        eventCount = {dhcp: 0, dns: 0, email: 0, firewall: 0, malware: 0, remotevpn: 0, webproxy: 0}; 
        Items.forEach(function(item) {
            var idName = item.dvc_type + "Wrapper";
            var className = item.dvc_type + "InputClass";
          
            // Add a row to the page if needed.
            if(eventCount[item.dvc_type] > 0) {
                element = document.getElementById(idName);
                element.insertAdjacentHTML('beforeend', '<div style="float:left;margin-right:20px;margin-top:25px;width:15%"><div><h2 style="vertical-align: bottom;" ></h2></div></div><div style="float:left;width:20%"><label for="ipaddr">IP Address</label><input class="' + className + '" id="ipaddr" type="text" value="" name="ipddr" readOnly="true"></input></div><div style="float:left;width:20%"><label for="nthost">NTHost</label><input class="' + className + '" id="nthost" type="text" value="" name="nthost" readOnly="true"></input></div><div style="float:left;width:20%"><label for="fqdn">FQDN</label><input class="' + className + '"id="fqdn" type="text" value="" name="fqdn" readOnly="true"></input><div style="float:left;width:20%"><input class="'+ className +'" id="_key" type="text" value="" name="_key" style="display: none;"></input></div></div><br style="clear:both;" />');
  
            }
            // Update Page with existing KVStore values.
            element = document.getElementById(idName).getElementsByClassName(className);
            element[eventCount[item.dvc_type] * 4].value = item.ip;
            element[eventCount[item.dvc_type] * 4 + 1].value = item.nt_host;
            element[eventCount[item.dvc_type] * 4 + 2].value = item.dns;
            element[eventCount[item.dvc_type] * 4 + 3].value = item._key;
          
            eventCount[item.dvc_type] = eventCount[item.dvc_type] + 1;
          
        });
        
        
// end of loadAssetPage function      
}

function assetsAddRow(divid) {
  var d1 = document.getElementById(divid);
  var className = divid.substr(0, divid.search("Wrapper")) + "InputClass";
  
  d1.insertAdjacentHTML('beforeend', '<div style="float:left;margin-right:20px;margin-top:25px;width:15%"><div><h2 style="vertical-align: bottom;" ></h2></div></div><div style="float:left;width:20%"><label for="ipaddr">IP Address</label><input class="' + className + '" id="ipaddr" type="text" value="" name="ipddr"></input></div><div style="float:left;width:20%"><label for="nthost">NTHost</label><input class="' + className + '" id="nthost" type="text" value="" name="nthost" ></input></div><div style="float:left;width:20%"><label for="fqdn">FQDN</label><input class="' + className + '"id="fqdn" type="text" value="" name="fqdn"></input><div style="float:left;width:20%"><input class="'+ className +'" id="_key" type="text" value="" name="_key" style="display: none;"></input></div></div><br style="clear:both;" />');
  
  
  return true;
}

function validationSearches() {
    
    require(['jquery',"splunkjs/mvc/utils","splunkjs/mvc/searchmanager"], function($, utils,SearchManager) {
      
      
      // test looping single code starts here.
      // Hard Coded Event Searches for now.
      splunkSearches = {DHCP: 'eventtype="dhcp_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(dest_nt_host) AS dest_nt_host dc(dest_mac) AS dest_mac dc(dest_ip) AS dest_ip | head 100',
                        DNS: 'eventtype="dns_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(src_ip) AS src_ip dc(query_type) AS query_type dc(query) AS query | head 100',
                        Email: 'eventtype="email_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(recipient) AS recipient dc(src_user) AS src_user dc(subject) AS subject dc(file_name) AS file_name dc(file_size) AS file_size | head 100',
                        Endpoint_IDS: 'eventtype="endpoint_ids_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(dest) AS dest dc(severity) AS severity dc(signature) AS signature dc(src_ip) AS src_ip dc(dest_port) AS dest_port dc(action) AS action | head 100',
                        Firewall: 'eventtype="firewall_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(action) AS action dc(src_ip) AS src_ip dc(src_port) AS src_port dc(dest_ip) AS dest_ip dc(dest_port) AS dest_port dc(bytes) AS bytes | head 100',
                        Host_Vulnerability: 'eventtype="host_vulnerability_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(dest) AS dest dc(severity) AS severity dc(signature) AS signature dc(mksb) AS mksb dc(cvss) AS cvss dc(cve) AS cve | head 100',
                        Malware: 'eventtype="malware_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(dest) AS dest dc(signature) AS signature dc(action) AS action | head 100',
                        Processes: 'eventtype="processes_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(parent_process) AS parent_process dc(process_id) AS process_id dc(process_name) AS process_name dc(command_line) AS command_line | head 100',
                        Remote_VPN: 'eventtype="remote_vpn_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(src_ip) AS src_ip dc(user) AS user dc(action) AS action dc(dest_ip) AS dest_ip | head 100',
                        Web_Traffic: 'eventtype="web_traffic_events" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(host) AS host dc(src_ip) AS src_ip dc(http_user_agent) AS http_user_agent dc(url) AS url dc(user) AS user dc(http_method) AS http_method dc(http_response) AS http_response dc(http_content_type) AS http_content_type | head 100',
                        Win_Priv_Access: 'eventtype="windows_privileged_access" | stats values(index) AS index values(sourcetype) AS sourcetype count AS Events dc(dest) AS dest dc(src_ip) AS src_ip dc(user) AS user dc(action) AS action dc(src_user) AS src_user | head 100'
                      };

      // Event Types we need to look for  
      events = ["DHCP","DNS","Email","Endpoint_IDS","Firewall","Host_Vulnerability","Malware","Processes","Remote_VPN","Web_Traffic","Win_Priv_Access"];
      
      // Show Progress Bar
      document.getElementById("validationdiv").style.display = "block";
      
      // Kick off eventtype searches
      for(i = 0; i < events.length; i++ ){
       
        searchID = events[i];    
        
        splunkQuery = splunkSearches[searchID];
        console.log('Processing Search: ', splunkQuery);
        
        // Destroy if the object is set
        if(typeof splunkjs.mvc.Components.getInstance(searchID) === "object") {
            console.log("Still have object set");
            splunkjs.mvc.Components.revokeInstance(searchID);
        }   
      
        // Create search manager      
        var searchID = new SearchManager ({
            "id": searchID,
            "cancelOnUnload": true,
            "latest_time": "",
            "status_buckets": 0,
            "earliest_time": "0",
            "search": splunkQuery,
            "app": utils.getCurrentApp(),
            "preview": true,
            "runWhenTimeIsUndefined": false,
            "autostart": true
        },
        { tokens: true, tokenNamespace: "submitted"  });
      
        searchID.on('search:error', function(properties) {
          searchError();
        });
        
        searchID.on('search:fail', function(properties) {
         searchFail();
        
        });
       
        searchID.on('search:done', function(properties) {
          
          //Update Progress Bar
          percComplete = (i/events.length)*100;
          percComplete = percComplete + "%";
          console.log("Percent Complete: ", percComplete);
     //     document.getElementById("validationProgress").style.width = percComplete;
     //     document.getElementById("validationProgressSpan").innerHTML = percComplete + " Complete";
          
          searchDone(properties);
        });
      
      //end of for loop
      }

    function searchError() {
        // console.log("errored", properties)
        $("#results").html("<p><span style=\"color: red; font-weight: bolder;\">ERROR!</span> Error running search! Check out the error in the Javascript Console.</p>");
        
    }
      
    function searchFail() {
      // console.log(searchName, "failed", properties)
      $("#results").html("<p><span style=\"color: red; font-weight: bolder;\">ERROR!</span> Search Failed! Check out the error in the Javascript Console.</p>"); 
    }
       
    function searchDone(properties) {
     
      var searchName = properties.content.request.label;
      var results = splunkjs.mvc.Components.getInstance(searchName).data('results', { output_mode: 'json', count: 0 });
      
      results.on("data", function(properties) {
               
        var searchName = properties.attributes.manager.id;
        //Table Title
        htmlResults = $('<h2 class="splunk-green">' + searchName + ' Events</h2>');
        
        var data = properties.data().results;
        console.log(searchName, "gave results", properties, data);
        
        // Update Progress Bar
        document.getElementById("validationProgress").style.width = percComplete;
        document.getElementById("validationProgressSpan").innerHTML = percComplete + " Complete";         
        
          switch(searchName) {
            case "DHCP":
              writeDHCP(data,searchName);
              return;
            case "DNS":
              writeDNS(data,searchName);
              return;
            case "Email":
              writeEmail(data,searchName);
              return;
            case "Endpoint_IDS":
              writeEndpointIDS(data,searchName);
              return;
            case "Firewall":
              writeFirewall(data,searchName);
              return;
            case "Host_Vulnerability":
              writeHostVulnerability(data,searchName);
              return;
            case "Malware":
              writeMalware(data,searchName);
              return;
            case "Processes":
              writeProcesses(data,searchName);
              return;
            case "Remote_VPN":
              writeRemoteVPN(data,searchName);
              return;
            case "Web_Traffic":
              writeWebTraffic(data,searchName);
              return;
            case "Win_Priv_Access":
              writeWinPrivAccess(data,searchName);
              return;
            
          
          // end of switch      
          }
        //end of results.on
        });
      
      setTimeout(function() { document.getElementById("validationdiv").style.display = "none"; }, 3000);
            
    // End of function  
    }   
       
       
    // End of Require   
    });
    
    
//End of function    
}

function prepFieldData(fieldStatus,searchName,field_has_events) {
  
  var fieldData = {};
  //Setup Field Status record
  fieldData.fields = fieldStatus;
  fieldData.event_type = searchName;
  fieldData.last_updated = Date.now();
  fieldData.has_events = field_has_events;
 
  // Get Existing Key if exists
  key = getFieldStatus(searchName);
  // Write to new or existing record. 
  writeFieldStatus(key,fieldData);
}

function getFieldStatus(searchName) {
  $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/field_status',
         async: false,
         success: function(returneddata)
         { Items = returneddata; } });
           
      key = undefined;
      Items.forEach(function(item) {
          if(item.event_type == searchName) {
            console.log("Matched Event Type");
            key = item._key;
          }
      });
      
  return(key);
}

function writeFieldStatus(key,fieldData) {
  
  var keyId = '';
  if(typeof(key) !== 'undefined') {
    keyId = '/' + key;
  }
           
  $.ajax({
        url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/field_status' + keyId,
        type: 'POST',
        contentType: "application/json",
        async: false,
        data: JSON.stringify(fieldData),
        success: function(returneddata) { newkey = returneddata; }
    });         
           
  return;
}

function writeWinPrivAccess(data,searchName) {
  console.log("Writing priv access Data with Name: ", searchName);
  console.log("Writing priv access Data with recordset: ", data);
  
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>dest</th><th>src_ip</th><th>user</th><th>action</th><th>src_ip</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgDest = "";
    bgSrcIP = "";
    bgUser = "";
    bgAction = "";
    bgSrcUser = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
    if(data[i].dest == 0) {
        bgDest = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "dest=0,";
    } else {
      bgDest = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
        fieldStatus = "dest=1,";
    }
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_ip=1,";
    }
    if(data[i].user == 0) {
        bgUser = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "user=0,";
    } else {
      bgUser = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "user=1,";
    }
    if(data[i].action == 0) {
        bgAction = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "action=0,";
    } else {
      bgAction = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "action=1,";
    }
    if(data[i].src_user == 0) {
        bgSrcUser = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_user=0,";
    } else {
      bgSrcUser = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_user=1,";
    }
    
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=win_priv_access_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgDest + '>' + data[i].dest +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgUser + '>' + data[i].user +
                                  "</td><td " + bgAction + '>' + data[i].action +
                                  "</td><td " + bgSrcUser + '>' + data[i].src_user +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#win_priv_access_results").html(htmlResults);
    
  // end of for loop
  }
  
  prepFieldData(fieldStatus,searchName,field_has_events);
  
//End of function    
}


function writeWebTraffic(data,searchName) {
  console.log("Writing Web Traffic Data with Name: ", searchName);
  console.log("Writing Web TrafficData with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>src_ip</th><th>http_user_agent</th><th>url</th><th>user</th><th>http_method</th><th>http_response</th><th>http_content_type</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgSrcIP = "";
    bgHTTPUserAgent = "";
    bgURL = "";
    bgUser= "";
    bgHTTPMethod = "";
    bgHTTPResponse = "";
    bgHTTPContentType = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].host == 0) {
        bgHost   = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_ip=1,";
    }
    if(data[i].http_user_agent == 0) {
        bgHTTPUserAgent = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "http_user_agent=0,";
    } else {
      bgHTTPUserAgent = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "http_user_agent=1,";
    }
    if(data[i].url == 0) {
        bgURL = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "url=0,";
    } else {
      bgURL = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "url=1,";
    }
    if(data[i].user == 0) {
        bgUser = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "user=0,";
    } else {
      bgUser = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "user=1,";
    }
    if(data[i].http_method == 0) {
        bgHTTPMethod = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "http_method=0,";
    } else {
      bgHTTPMethod = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "http_method=1,";
    }
    if(data[i].http_response == 0) {
        bgHTTPResponse = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "http_response=0,";
    } else {
      bgHTTPResponse = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "http_response=1,";
    }
    if(data[i].http_content_type == 0) {
        bgHTTPContentType = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "http_content_type=0,";
    } else {
      bgHTTPContentType = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "http_content_type=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=web_traffic_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgHTTPUserAgent + '>' + data[i].http_user_agent +
                                  "</td><td " + bgURL + '>' + data[i].url +
                                  "</td><td " + bgUser + '>' + data[i].user +
                                  "</td><td " + bgHTTPMethod + '>' + data[i].http_method +
                                  "</td><td " + bgHTTPResponse + '>' + data[i].http_response +
                                  "</td><td " + bgHTTPContentType + '>' + data[i].http_content_type +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#web_traffic_results").html(htmlResults);
    
  // end of for loop
  }
  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeRemoteVPN(data,searchName) {
  console.log("Writing Remote VPN Data with Name: ", searchName);
  console.log("Writing Remote VPN Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>src_ip</th><th>user</th><th>dest_ip</th><th>action</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgSrcIP = "";
    bgUser = "";
    bgAction = "";
    bgDestIP = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].host == 0) {
        bgHost   = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_ip=1,";
    }
    if(data[i].user == 0) {
        bgUser = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "user=0,";
    } else {
      bgUser = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "user=1,";
    } 
    if(data[i].action == 0) {
        bgAction = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "action=0,";
    } else {
      bgAction = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "action=1,";
    }
    if(data[i].dest_ip == 0) {
        bgDestIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest_ip=0,";
    } else {
      bgDestIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest_ip=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=remote_vpn_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgUser + '>' + data[i].user +
                                  "</td><td " + bgAction + '>' + data[i].action +
                                  "</td><td " + bgDestIP + '>' + data[i].dest_ip +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#remote_vpn_results").html(htmlResults);
    
  // end of for loop
  }
  
  prepFieldData(fieldStatus,searchName,field_has_events);
  
//End of function    
}

function writeProcesses(data,searchName) {
  console.log("Writing Processes Data with Name: ", searchName);
  console.log("Writing Processes Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>parent_process</th><th>process_id</th><th>process_name</th><th>command_line</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgParentProcess = "";
    bgProcessID = "";
    bgProcessName = "";
    bgCommandLine = "";
                    
    if(data[i].Events == 0) {
        bgEvents = '<a href=\"#\" title="I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) ' +
        'configured correctly." data-placement="top" data-content=\"No Content\" ' +
        'data-trigger="hover" data-toggle="popover"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>"';
        field_has_events = 0;
       
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
    if(data[i].host == 0) {
        bgHost   = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].parent_process == 0) {
        bgParentProcess = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "parent_process=0,";
    } else {
      bgParentProcess = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "parent_process=1,";
    }
    if(data[i].process_id == 0) {
        bgProcessID = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "process_id=0,";
    } else {
      bgProcessID = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "process_id=1,";
    }
    if(data[i].process_name == 0) {
        bgProcessName = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "process_name=0,";
    } else {
      bgProcessName = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "process_name=1,";
    }
    if(data[i].command_line == 0) {
        bgCommandLine = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "command_line=0,";
    } else {
      bgCommandLine = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "command_line=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=process_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgParentProcess + '>' + data[i].parent_process +
                                  "</td><td " + bgProcessID + '>' + data[i].process_id +
                                  "</td><td " + bgProcessName + '>' + data[i].process_name +
                                  "</td><td " + bgCommandLine + '>' + data[i].command_line +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#process_results").html(htmlResults);
    
  // end of for loop
  }
  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeMalware(data,searchName) {
  console.log("Writing Malware Data with Name: ", searchName);
  console.log("Writing Malware Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>dest</th><th>signature</th><th>action</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgDest = "";
    bgSignature = "";
    bgAction = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].host == 0) {
        bgHost   = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].dest == 0) {
        bgDest = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest=0,";
    } else {
      bgDest = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest=1,";
    }
    if(data[i].signature == 0) {
        bgSignature = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "signature=0,";
    } else {
      bgSignature = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "signature=1,";
    }
    if(data[i].action == 0) {
        bgAction = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "action=0,";
    } else {
      bgAction  = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "action=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=malware_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgDest + '>' + data[i].dest +
                                  "</td><td " + bgSignature + '>' + data[i].signature +
                                  "</td><td " + bgAction + '>' + data[i].action +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#malware_results").html(htmlResults);
    
  // end of for loop
  }
  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeHostVulnerability(data,searchName) {
  console.log("Writing Host Vulnerability Data with Name: ", searchName);
  console.log("Writing Host Vulnerability Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>dest</th><th>severity</th><th>signature</th><th>mksb</th><th>cvss</th><th>cve</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgDest = "";
    bgSeverity = "";
    bgSignature = "";
    bgMKSB = "";
    bgCVSS = "";
    bgCVE = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].dest == 0) {
        bgDest = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "dest=0,";
    } else {
      bgDest = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "dest=1,";
    }
    if(data[i].severity == 0) {
        bgSeverity = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "severity=0,";
    } else {
      bgSeverity = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "severity=1,";
    }
    if(data[i].signature == 0) {
        bgSignature = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "signature=0,";
    } else {
      bgSignature = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "signature=1,";
    }
    if(data[i].mksb == 0) {
        bgMKSB = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "mksb=0,";
    } else {
      bgMKSB = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "mksb=0,";
    }
    if(data[i].cvss == 0) {
        bgCVSS = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "cvss=0,";
    } else {
      bgCVSS = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "cvss=1,";
    }
    if(data[i].cve == 0) {
        bgCVE  = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "cve=0,";
    } else {
      bgCVE = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "cve=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=host_vulnerability_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgDest + '>' + data[i].dest +
                                  "</td><td " + bgSeverity + '>' + data[i].severity +
                                  "</td><td " + bgSignature + '>' + data[i].signature +
                                  "</td><td " + bgMKSB + '>' + data[i].mksb +
                                  "</td><td " + bgCVSS + '>' + data[i].cvss +
                                  "</td><td " + bgCVE + '>' + data[i].cve +
                                   "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#host_vulnerability_results").html(htmlResults);
    
  // end of for loop
  }

  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeFirewall(data,searchName) {
  console.log("Writing Firewall Data with Name: ", searchName);
  console.log("Writing Firewall Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>action</th><th>src_ip</th><th>src_port</th><th>dest_ip</th><th>dest_port</th><th>bytes</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgAction = "";
    bgSrcIP = "";
    bgSrcPort = "";
    bgDestIP = "";
    bgDestPort = "";
    bgBytes = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].action == 0) {
        bgAction = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "action=0,";
    } else {
        bgAction = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
        fieldStatus = "action=1,";
    }
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus  + "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus  + "src_ip=1,";
    }
    if(data[i].src_port == 0) {
        bgSrcPort = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus  + "src_port=0,";
    } else {
      bgSrcPort = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus  + "src_port=1,";
    }
    if(data[i].dest_ip == 0) {
        bgDestIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus  + "dest_ip=0,";
    } else {
      bgDestIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus  + "dest_ip=1,";
    }
    if(data[i].dest_port == 0) {
        bgDestPort = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus  + "dest_port=0,";
    } else {
      bgDestPort = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus  + "dest_port=1,";
    }
    if(data[i].bytes == 0) {
        bgBytes  = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus  + "bytes=0,";
    } else {
      bgBytes = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus  + "bytes=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=firewall_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgDest + '>' + data[i].action +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgSrcPort + '>' + data[i].src_port +
                                  "</td><td " + bgDestIP + '>' + data[i].dest_ip +
                                  "</td><td " + bgDestPort + '>' + data[i].dest_port +
                                  "</td><td " + bgBytes + '>' + data[i].bytes +
                                   "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#firewall_results").html(htmlResults);
    
  // end of for loop
  }
  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeEndpointIDS(data,searchName) {
  console.log("Writing Endpoint IDS Data with Name: ", searchName);
  console.log("Writing Endpoint IDS Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>dest</th><th>severity</th><th>signature</th><th>src_ip</th><th>dest_port</th><th>action</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgDest = "";
    bgSeverity = "";
    bgSignature = "";
    bgSrcIP = "";
    bgDestPort = "";
    bgAction = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
   
    if(data[i].dest == 0) {
        bgDest = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "dest=0,";
    } else {
      bgDest = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "dest=1,";
    }
    if(data[i].severity == 0) {
        bgSeverity = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "severity=0,";
    } else {
      bgSeverity = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "severity=1,";
    }
    if(data[i].signature == 0) {
        bgSignature = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "signature=0,";
    } else {
      bgSignature = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "signature=1,";
    }
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_ip=1,";
    }
    if(data[i].dest_port == 0) {
        bgDestPort = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest_port=0,";
    } else {
      bgDestPort = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest_port=1,";
    }
    if(data[i].action == 0) {
        bgAction = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "action=0,";
    } else {
      bgAction = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "action=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=endpoint_ids_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgDest + '>' + data[i].dest +
                                  "</td><td " + bgSeverity + '>' + data[i].severity +
                                  "</td><td " + bgSignature + '>' + data[i].signature +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgDestPort + '>' + data[i].dest_port +
                                  "</td><td " + bgAction + '>' + data[i].action +
                                   "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#endpoint_ids_results").html(htmlResults);
    
  // end of for loop
  }
  prepFieldData(fieldStatus,searchName,field_has_events);
//End of function    
}

function writeEmail(data,searchName) {
  console.log("Writing EMail Data with Name: ", searchName);
  console.log("Writing EMail Data with recordset: ", data);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>recipient</th><th>src_user</th><th>subject</th><th>file_name</th><th>file_size</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgRecipient = "";
    bgSrcUser = "";
    bgSubject = "";
    bgFileName = "";
    bgFileSize = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
    if(data[i].host == 0) {
        bgHost = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].recipient == 0) {
        bgRecipient = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "recipient=0,";
    } else {
      bgRecipient = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "recipient=0,";
    }
    if(data[i].src_user == 0) {
        bgSrcUser = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "src_user=0,";
    } else {
      bgSrcUser = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "src_user=1,";
    }
    if(data[i].subject == 0) {
        bgSubject = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "subject=0,";
    } else {
      bgSubject = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "subject=1,";
    }
    if(data[i].file_name == 0) {
        bgFileName = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "file_name=0,";
    } else {
      bgFileName = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "file_name=1,";
    }
    if(data[i].file_size == 0) {
        bgFileSize = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "file_size=0,";
    } else {
      bgFileSize = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "file_size=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=email_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgRecipient + '>' + data[i].recipient +
                                  "</td><td " + bgSrcUser + '>' + data[i].src_user +
                                  "</td><td " + bgSubject + '>' + data[i].subject +
                                  "</td><td " + bgFileName + '>' + data[i].file_name +
                                  "</td><td " + bgFileSize + '>' + data[i].file_size +
                                   "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#email_results").html(htmlResults);
    
  // end of for loop
  }
    prepFieldData(fieldStatus,searchName,field_has_events);
}


function writeDNS(data,searchName) {
  console.log("Writing DNS Data with recordset: ", searchName);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>src_ip</th><th>query_type</th><th>query</th><th style="float: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgSrcIP = "";
    bgQueryType = "";
    bgQuery = "";
                    
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    } 
    if(data[i].src_ip == 0) {
        bgSrcIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "src_ip=0,";
    } else {
      bgSrcIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "src_ip=1,";
    }
    if(data[i].query_type == 0) {
        bgQueryType = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "query_type=0,";
    } else {
      bgQueryType = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "query_type=1,";
    }
    if(data[i].query == 0) {
        bgQuery = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "query=0,";
    } else {
      bgQuery = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "query=1,";
    }
    
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open in Search</a></html>";
                  
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgSrcIP + '>' + data[i].src_ip +
                                  "</td><td " + bgQueryType + '>' + data[i].query_type +
                                  "</td><td " + bgQuery + '>' + data[i].query +
                                   "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );

    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#dns_results").html(htmlResults);
    
  // end of for loop
  }
    prepFieldData(fieldStatus,searchName,field_has_events);
// end of function    
}
function writeDHCP(data,searchName) {
  console.log(searchName);
  for (var i = 0; i < data.length; i++) {
    
    if(i == 0) {
      // Table Header
        htmlResults.append('<table class="table table-striped table-hover" style="color: white;"><thead><tr><th>Index</th><th>sourcetype</th><th>Events</th><th>host</th><th>dest_nt_host</th><th>dest_mac</th><th>dest_ip</th><th style="text-align: right;">Extract Fields</th></tr></thead><tbody></tbody></table>');
    }
    
    bgEvents = "";
    bgHost = "";
    bgDestNtHost = "";
    bgDestMac = "";
    bgDestIP = "";
                   
    if(data[i].Events == 0) {
        bgEvents = "<a href=\"#\" title=\"I didn\'t find any data. Check you have some, have the right technology add on installed and Eventtype (stage 3) " +
        "configured correctly.\" data-placement=\"top\" data-content=\"No Content\" " +
        "data-trigger=\"hover\" data-toggle=\"popover\"><i class=\"icon-info-circle\" style=\"color: #00a4fd; text-align: center; font-size: 25px;\" /></a>";
        field_has_events = 0;
    } else {
        //bgEvents = data[i].Events;
        bgEvents = "<i class=\"icon-check-circle\" style=\"text-align: center; font-size: 25px; color: green;\" />";
        field_has_events = 1;
    }
    
    if(data[i].host == 0) {
        bgHost = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = "host=0,";
    } else {
      bgHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = "host=1,";
    }
    if(data[i].dest_nt_host == 0) {
        bgDestNtHost = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest_nt_host=0,";
    } else {
      bgDestNtHost = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest_nt_host=1,";
    }
    if(data[i].dest_mac == 0) {
        bgDestMac = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest_mac=0,";
    } else {
      bgDestMac = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest_mac=1,";
    }
    if(data[i].dest_ip == 0) {
        bgDestIP = "<i class=\"icon-x-circle\" style=\"color: red; align: center; font-size: 25px;\" />";
        fieldStatus = fieldStatus + "dest_ip=0,";
    } else {
      bgDestIP = "<i class=\"icon-check-circle\" style=\"align: center; font-size: 25px; color: green;\" />";
      fieldStatus = fieldStatus + "dest_ip=1,";
    }
    button = "<html><a target=\"_blank\" href=\"search?q=eventtype=dhcp_events&amp;earliest=-24h@h&amp;latest=now\" class=\"btn btn-link\" type=\"button\">Open In Search</a></html>";
    
    htmlResults.find("tbody").append("<tr><td> " + data[i].index + 
                                  "</td><td> " + data[i].sourcetype +
                                  "</td><td> " + bgEvents  +
                                  "</td><td " + bgHost + '>' + data[i].host +
                                  "</td><td " + bgDestNtHost + '>' + data[i].dest_nt_host +
                                  "</td><td " + bgDestMac + '>' + data[i].dest_mac +
                                  "</td><td " + bgDestIP + '>' + data[i].dest_ip +
                                  "</td><td style=\"text-align: right;\"" + button +
                                  "</td></tr>" );
                             
    //if(createButton) {
    //    htmlResults.append('<a target=\"_blank\" href=\"search?q=eventtype=dns_events&amp;earliest=-24h@h&amp;latest=now\" id="createDNSFields" class="btn btn-primary" type="button">Extract New Fields</a>');
    //}
    
    $("#dhcp_results").html(htmlResults);
  // end of for loop
  }
  
  prepFieldData(fieldStatus,searchName,field_has_events);
  
  //end of function
}

function checkDependancies() {
    var alert_manager_items = [];
    var parallel_items = [];
    var alert_addon_items;
    var taLink;
    
    // Checking for Alert Manager APP
    $.ajax({ url: '/static/app/alert_manager/alert_status.css', async: false, success: function(returneddata) { alert_manager_items = returneddata; } });
    
    
    if(alert_manager_items.length == 0 ) {
        //console.log("Appears Alert Manager is NOT installed");
        document.getElementById("alertManagerInstalled").className = "icon-x-circle splunk-red";
        document.getElementById("alertManagerInstalled").style.fontSize = "20px";
        taLink = "https://splunkbase.splunk.com/app/2665/";
        document.getElementById("alertManagerMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
    } else {
        //console.log("Appears Alert Manager IS installed - YAY");
        document.getElementById("alertManagerInstalled").className = "icon-check-circle splunk-green";
        document.getElementById("alertManagerInstalled").style.fontSize = "20px";
        document.getElementById("alertManagerMessage").innerHTML = "<p>Passed... </p>";
        
    }
    
    // Checking for Parallel Co-Ordinates Visualisation
    $.ajax({ url: '/static/app/parallel_coordinates_app/visualizations/parallel_coordinates/package.json', async: false, success: function(returneddata) { parallel_items = returneddata; } });
    
    if(parallel_items.length == 0 ) {
        //console.log("Appears Parallel Co-ords is NOT installed");
        document.getElementById("parallelCoordsInstalled").className = "icon-x-circle splunk-red";
         document.getElementById("parallelCoordsInstalled").style.fontSize = "20px";
        taLink = "https://splunkbase.splunk.com/app/3137/";
        document.getElementById("parallelCoordsMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
    } else {
        //console.log("Appears Parallel Co-ords IS installed - YAY");
        document.getElementById("parallelCoordsInstalled").className = "icon-check-circle splunk-green";
        document.getElementById("parallelCoordsInstalled").style.fontSize = "20px";
        document.getElementById("parallelCoordsMessage").innerHTML = "<p>Passed... </p>";
    }
        
    // Checking for the TA is not so simple, since it places nothing in appserver/static like the above.
    // Going long and create a search manager to do this. 
    require(["jquery","underscore","splunkjs/mvc","splunkjs/mvc/searchmanager"],function($,_,mvc,SearchManager) {
       
        // Destroy any previous instances of object - so we can re run searches.
        var checkAlertManagerTAInstalled;
        if(typeof splunkjs.mvc.Components.getInstance(checkAlertManagerTAInstalled) == "object")
           {
            splunkjs.mvc.Components.revokeInstance(checkAlertManagerTAInstalled);
           }
        
            var checkTAInstalled = new SearchManager({
                "id": "checkAlertManagerTAInstalled",
                "cancelOnUnload": true,
                "app": "security_monitoring_for_splunk",
                "autostart": false
                },
                {
                tokens: true,
                tokenNamespace: "submitted"  
                });
            
            var results = checkTAInstalled.data('results', {output_mode: 'json', count: 0});
            var searchQuery = "| rest /servicesNS/-/TA-alert_manager/data/props";
            var theSearch = mvc.Components.getInstance("checkAlertManagerTAInstalled");
            //Start the Search
            theSearch.set("search", searchQuery);
            theSearch.startSearch();
            
            // Search Events
            checkTAInstalled.on('search:error', function(properties) {
                console.log("Search Error", properties);
            });
            
            checkTAInstalled.on('search:done', function(properties) {
               alert_addon_items = properties.content.resultCount;
               
                    if(alert_addon_items == 0 ) {
                        document.getElementById("alertAddonInstalled").className = "icon-x-circle splunk-red";
                        document.getElementById("alertAddonInstalled").style.fontSize = "20px";
                        var taLink = "https://splunkbase.splunk.com/app/3365/";
                        document.getElementById("alertAddonMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
                    } else {
                        //console.log("Appears alert addon IS installed - YAY");
                        document.getElementById("alertAddonInstalled").className = "icon-check-circle splunk-green";
                        document.getElementById("alertAddonInstalled").style.fontSize = "20px";
                        document.getElementById("alertAddonMessage").innerHTML = "<p>Passed... </p>";
                    }    
            });
            
            
            checkTAInstalled.on('data', function(properties) {
                console.log("Have Data:", results.data().results);    
            });
   
   
        // Destroy any previous instances of object - so we can re run searches.
        var checkurlparserInstalled;
        if(typeof splunkjs.mvc.Components.getInstance(checkurlparserInstalled) == "object")
           {
            splunkjs.mvc.Components.revokeInstance(checkurlparserInstalled);
           }
        
            checkurlparserInstalled = new SearchManager({
                "id": "checkurlparserInstalled",
                "cancelOnUnload": true,
                "app": "security_monitoring_for_splunk",
                "autostart": false
                },
                {
                tokens: true,
                tokenNamespace: "submitted"  
                });
            
            results = checkurlparserInstalled.data('results', {output_mode: 'json', count: 0});
            searchQuery = "| rest /servicesNS/-/urlparser/data/commands";
            theSearch = mvc.Components.getInstance("checkurlparserInstalled");
            //Start the Search
            theSearch.set("search", searchQuery);
            theSearch.startSearch();
            
            // Search Events
            checkurlparserInstalled.on('search:error', function(properties) {
                console.log("Search Error", properties);
            });
            
            checkurlparserInstalled.on('search:done', function(properties) {
               urlparser_items = properties.content.resultCount;
               
                    if(urlparser_items == 0 ) {
                        document.getElementById("urlparserInstalled").className = "icon-x-circle splunk-red";
                        document.getElementById("urlparserInstalled").style.fontSize = "20px";
                        var taLink = "https://splunkbase.splunk.com/app/3396/";
                        document.getElementById("urlparserMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
                    } else {
                        //console.log("Appears alert addon IS installed - YAY");
                        document.getElementById("urlparserInstalled").className = "icon-check-circle splunk-green";
                        document.getElementById("urlparserInstalled").style.fontSize = "20px";
                        document.getElementById("urlparserMessage").innerHTML = "<p>Passed... </p>";
                    }    
            });
            
            
            checkurlparserInstalled.on('data', function(properties) {
                console.log("Have Data:", results.data().results);    
            });
   
         var checkhorizonchartInstalled;
         if(typeof splunkjs.mvc.Components.getInstance(checkhorizonchartInstalled) == "object")
           {
            splunkjs.mvc.Components.revokeInstance(checkhorizonchartInstalled);
           }
        
            checkhorizonchartInstalled = new SearchManager({
                "id": "checkhorizonchartInstalled",
                "cancelOnUnload": true,
                "app": "security_monitoring_for_splunk",
                "autostart": false
                },
                {
                tokens: true,
                tokenNamespace: "submitted"  
                });
            
            results = checkhorizonchartInstalled.data('results', {output_mode: 'json', count: 0});
            searchQuery = "| rest /servicesNS/-/horizon_chart_app/data/commands";
            theSearch = mvc.Components.getInstance("checkhorizonchartInstalled");
            //Start the Search
            theSearch.set("search", searchQuery);
            theSearch.startSearch();
            
            // Search Events
            checkhorizonchartInstalled.on('search:error', function(properties) {
                console.log("Search Error", properties);
            });
            
            checkhorizonchartInstalled.on('search:done', function(properties) {
               horizonchart_items = properties.content.resultCount;
               
                    if(horizonchart_items == 0 ) {
                        document.getElementById("horizonchartInstalled").className = "icon-x-circle splunk-red";
                        document.getElementById("horizonchartInstalled").style.fontSize = "20px";
                        var taLink = "https://splunkbase.splunk.com/app/3117/";
                        document.getElementById("horizonchartMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
                    } else {
                        //console.log("Appears alert addon IS installed - YAY");
                        document.getElementById("horizonchartInstalled").className = "icon-check-circle splunk-green";
                        document.getElementById("horizonchartInstalled").style.fontSize = "20px";
                        document.getElementById("horizonchartMessage").innerHTML = "<p>Passed... </p>";
                    }    
            });
            
            
            checkhorizonchartInstalled.on('data', function(properties) {
                console.log("Have Data:", results.data().results);    
            });
            
            
            // Checking for Event TimeLine APP
            $.ajax({ url: '/static/app/event-timeline-viz/table2list.js', async: false,
                   success: function(returneddata) { event_timeline = returneddata; },
                   error: function() { event_timeline = "error" ;  }
            });
            
    
            if(event_timeline.length == 0 || event_timeline == 'error' ) {
             //console.log("Appears TImeline Viz is NOT installed");
                document.getElementById("timelineVizInstalled").className = "icon-x-circle splunk-red";
                document.getElementById("timelineVizInstalled").style.fontSize = "20px";
                taLink = "https://splunkbase.splunk.com/app/4370/";
                document.getElementById("timelineVizMessage").innerHTML = "<p>Not Installed. <a href=" + taLink + "  target=\"_blank\">Download</a></p>";
            } else {
                //console.log("Appears Alert Manager IS installed - YAY");
                document.getElementById("timelineVizInstalled").className = "icon-check-circle splunk-green";
                document.getElementById("timelineVizInstalled").style.fontSize = "20px";
                document.getElementById("timelineVizMessage").innerHTML = "<p>Passed... </p>";
            }
  });
        
}

function getSplunkbaseLink(input) {
    var splunkbaseLink;
    var outputID;
    
    if(input == "windows") {
        splunkbaseLink = windowsSplunkbase();
        outputID = "taWindowsOutput";
    } else if(input == "firewall") {
        splunkbaseLink = firewallSplunkbase();
        outputID = "taFirewallOutput";
    } else if(input == "webProxy") {
        splunkbaseLink = webProxySplunkbase();
        outputID = "taWebProxyOutput";
    } else if(input == "emailProxy") {
        splunkbaseLink = emailProxySplunkbase();
        outputID = "taEmailProxyOutput";
    } else if(input == "dhcp") {
        splunkbaseLink = dhcpSplunkbase();
        outputID = "taDHCPOutput";
    } else if(input == "dns") {
        splunkbaseLink = dnsSplunkbase();
        outputID = "taDNSOutput";
    } else if(input == "networkIDS") {
        splunkbaseLink = networkIDSSplunkbase();
        outputID = "taNetworkIDSOutput";
    } else if(input == "vpn") {
        splunkbaseLink = vpnSplunkbase();
        outputID = "taVPNOutput";
    } else if(input == "malware") {
        splunkbaseLink = malwareSplunkbase();
        outputID = "taMalwareOutput";
    } else if(input == "processExecution") {
        splunkbaseLink = processExecutionSplunkbase();
        outputID = "taProcessExecutionOutput";
    } else if(input == "vulnerability") {
        splunkbaseLink = vulnerabilitiesSplunkbase();
        outputID = "taVulnerabilitiesOutput";
    } else if(input == "hostIDS") {
        splunkbaseLink = hostIDSSplunkbase();
        outputID = "taHostIDSOutput";
    } else {
        console.log("Something Went Badly Wrong...");
    }
    
    
    if(splunkbaseLink === undefined) { splunkbaseLink = "No Splunkbase link found."; }
    console.log("Setting output to ", splunkbaseLink);
    document.getElementById(outputID).innerHTML = "<p>" + splunkbaseLink + "<a href=" + splunkbaseLink + "  target=\"_blank\">&nbsp;Download <i class=\"icon-external\"></i></a></p>";
}

function hostIDSSplunkbase() {
    switch ($('#taHostIDSInput').val()) {
     case "symantec":
        splunkbaseLink = "https://splunkbase.splunk.com/app/2772/";
        return(splunkbaseLink);
    case "sophos":
        splunkbaseLink = "https://splunkbase.splunk.com/app/1854/";
        return(splunkbaseLink);
    }
}


function vulnerabilitiesSplunkbase() {
    switch ($('#taVulnerabilitiesInput').val()) {
        case "tenable":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1710/";
            return(splunkbaseLink);
        case "rapid7":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3457/";
            return(splunkbaseLink);
    }
}

function processExecutionSplunkbase() {
    switch ($('#taProcessExecutionInput').val()) {
        case "winsecurity":
            splunkbaseLink = "https://splunkbase.splunk.com/app/742/";
            return(splunkbaseLink);
        case "sysmon":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1914/";
            return(splunkbaseLink);
        case "symantec":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2772/";
            return(splunkbaseLink);
    }
}


function malwareSplunkbase() {
    switch ($('#taMalwareInput').val()) {
        case "crowdstrike":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1914/";
            return(splunkbaseLink);
        case "symantec-ep":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3121/";
            return(splunkbaseLink);
        case "windowsdefender":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3734/";
            return(splunkbaseLink);
        case "cbresponse":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3545/";
            return(splunkbaseLink);
        case "mcafee":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1819/";
            return(splunkbaseLink);
        case "eset":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3867/";
            return(splunkbaseLink);
        case "sophos":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1854/";
            return(splunkbaseLink);
    }
    
}

function vpnSplunkbase() {
    switch ($('#taVPNInput').val()) {
        case "fortinet":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2846/";
            return(splunkbaseLink);
        case "juniper":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2847/";
            return(splunkbaseLink);
    }
}
function networkIDSSplunkbase() {
    switch ($('#taNetworkIDSInput').val()) {
        case "surricata":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2760/";
            return(splunkbaseLink);
        case "zeek-ids":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1617/";
            return(splunkbaseLink);
    }
}

function dhcpSplunkbase() {
    switch ($('#taDHCPInput').val()) {
     case "windows-dhcp":
        splunkbaseLink = "https://splunkbase.splunk.com/app/742/";
        return(splunkbaseLink);
    }
}

function dnsSplunkbase() {
    switch ($('#taDNSInput').val()) {
        case "windows-dns":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3377";
            return(splunkbaseLink);
        case "bind-dns":
            splunkbaseLink = "sorry - dont know";
            return(splunkbaseLink);
        case "zeek-dns":
            splunkbaseLink = "sorry - dont know";
            return(splunkbaseLink);
        case "infoblox-dns":
            splunkbaseLink = "sorry - dont know";
            return(splunkbaseLink);
    }
}

function emailProxySplunkbase() {
    switch ($('#taEmailProxyInput').val()) {
        case "proofpoint":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3080/";
            return(splunkbaseLink);
        case 'cisco-esa':
            splunkbaseLink = "https://splunkbase.splunk.com/app/1761/";
            return(splunkbaseLink);
        case "symantec-ep":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3831/";
            return(splunkbaseLink);
    }
    
}

function webProxySplunkbase() {
    switch ($('#taWebProxyInput').val()) {
        case "cisco-wsa":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1747/";
            return(splunkbaseLink);
    }
    
}

function windowsSplunkbase() {
    switch ($('#taWindowsInput').val()) {
        case "yes":
            splunkbaseLink = "https://splunkbase.splunk.com/app/742/";
            return(splunkbaseLink);
        case "no":
            splunkbaseLink = "https://splunkbase.splunk.com/app/742/";
            return(splunkbaseLink);
    }
    
}

function firewallSplunkbase() {
    
    switch ($('#taFirewallInput').val()) {
        case "checkpoint":
            splunkbaseLink = "https://splunkbase.splunk.com/app/3197/";
            return(splunkbaseLink);   
        case "cisco":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1467/";
            return(splunkbaseLink);
        case "palo-alto":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2757/";
            return(splunkbaseLink);
         case "pfsense":
            splunkbaseLink = "https://splunkbase.splunk.com/app/1527/";
            return(splunkbaseLink);
        case "fortinet":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2846/";
            return(splunkbaseLink);
        case "juniper":
            splunkbaseLink = "https://splunkbase.splunk.com/app/2847/";
            return(splunkbaseLink);
    }
}

$("#stepWizardFinish").click(function(e) {
   e.preventDefault();
   
   finalize_tasks();
   
   var record = {};
   last_ran = new Date();
   record.last_ran = last_ran.getTime();
   //record.last_ran = new Date();
   record.setup_ran = 1;
   
   
   $.ajax({
            url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/global_config',
            type: 'POST',
            contentType: "application/json",
            async: false,
            data: JSON.stringify(record),
            success: function(returneddata) { newkey = returneddata; console.log("Written to global_config", newkey);}
        });
   
   window.location.href = "/app/security_monitoring_for_splunk/system_status";
});

function stepWizard() {
    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allWells.hide();
     
    navListItems.click(function (e) {
        e.preventDefault();

        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {

            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
            
        }
    });

    allNextBtn.click(function () {
      
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;
          
            if (curStepBtn == "step-4") {    
              loadAssetPage();
            }
                   
        $(".form-group").removeClass("has-error");

        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
              
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        
        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        
    });
    
    $('div.setup-panel div a.btn-primary').trigger('click');
    
    }
    
function finalize_tasks() {
  
  // Populate Playbook Unique Numbers with default values.
  // Check to see if the collection already exists.
  console.log("Setting up Playbook Numbers");
  $.ajax({ url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num?output_mode=json',
         async: false, success: function(returneddata) { unique_nums = returneddata; } });
    
    // If unique_num hasn't been initialised yet - initialise it.
    if(typeof unique_nums == "undefined" || unique_nums == "") {
      console.log("Reckon Empty Records Found.");
      var nums = {};
      nums = {"dhcp":1000, 
          "dns": 2000,
          "email":3000,
          "endpoint_ids":4000,
          "firewall":5000,
          "host_vulnerability":6000,
          "malware":7000,
          "network_ids":8000,
          "plug_and_play":9000,
          "processes":10000,
          "remote_vpn":11000,
          "web_traffic":12000,
          "windows_security":13000,
          "linux_security":14000
         };
      
      entries = Object.entries(nums);
      var record = {};
      for ( var [event,count] of entries) {
         console.log("Entry:", event, "Count:", count);
         record.event_source = event;
         record.last_num = count;
         
         $.ajax({
            url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num',
            type: 'POST',
            contentType: "application/json",
            async: false,
            data: JSON.stringify(record),
            success: function(returneddata) { newkey = returneddata; }
         });    
      }
    }
  
   // installing playbooks.
   //var playbooks = new Object();
   console.log("Importing Playbooks");
   var playbooks = {};
   $.ajax({ url: '/static/app/security_monitoring_for_splunk/config/playbook.json?output_mode=json',
          async: false,
          success: function(returneddata) { playbooks = returneddata; },
          error: function(returneddata) { console.log("Error:", returneddata); }
          });
      
      var entry = {};
      for (i=0; i<playbooks.length; i++) {
         console.log("playbooks:", playbooks[i].unique_id);
         console.log("playbooks:", playbooks[i].description);
         entry.description = playbooks[i].description;
         entry.analysts_comments = playbooks[i].analysts_comments;
         entry.results_analysis = playbooks[i].results_analysis;
         entry.event_source = playbooks[i].event_source;
         entry.fidelity = playbooks[i].fidelity;
         entry.last_edited_by = playbooks[i].last_edited_by;
         entry.mitre_tactic = playbooks[i].mitre_tactic;
         entry.mitre_technique = playbooks[i].mitre_technique;
         entry.objective = playbooks[i].objective;
         entry.owner = playbooks[i].owner;
         entry.report_category = playbooks[i].report_category;
         entry.rev_num = playbooks[i].rev_num;
         entry.splunk_search = playbooks[i].splunk_search;
         entry.tags = playbooks[i].tags;
         entry._time = new Date().getTime() / 1000;
         entry.date_last_edited = 0;
         entry.playbook_num = playbooks[i].playbook_num;
         entry.name = playbooks[i].name;
   
         // Check if the Playbook ID exists inthe KVStore already
         
         var filter = "{\"playbook_num\": " + entry.playbook_num + "}";
         console.log("Filter Set to:",filter);
         $.ajax({
          url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook?query=' +encodeURIComponent(filter),
          type: 'GET',
          contentType: "application/json",
          async: false,
          data: JSON.stringify(obj),
          success: function(returneddata) { exists = returneddata; }
         });
         console.log("Exists = ", exists);
   
         // Does not exist already - Add playbook to KVSTORE  
         if(exists == 0) {
            console.log("playbook does not exist - creating");
               // Get Latest Unique Number and update
                $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num',
                   async: false, success: function(returneddata) { Items = returneddata; } });
            
               var obj = Items.find(o => o.event_source.toString().toLowerCase() === entry.event_source.toString().toLowerCase());
            
               if(typeof obj == "undefined") {
                status = 1;
                return status;
               }
               
               var newNum = Number(obj.last_num) + 1;
               obj.last_num = newNum;
               
               // Create unique String
               //entry.unique_id = newNum + '-' + entry.fidelity + '-' + entry.name + '-' + entry.report_category;
               entry.unique_id = newNum + '-' + entry.fidelity + '-' + entry.name;
            
               $.ajax({
                  url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num/' +encodeURIComponent(obj._key),
                  type: 'POST',
                  contentType: "application/json",
                  async: false,
                  data: JSON.stringify(obj),
                  success: function(returneddata) { newkey = returneddata; }
               });
         
               // AJAX call to POST each entry in turn..
               $.ajax({
                  url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook',
                  type: 'POST',
                  contentType: "application/json",
                  async: false,
                  data: JSON.stringify(entry),
                  success: function(returneddata) { newkey = returneddata; }
               });
               
               
               
         // End if
         }
      // End for
      }
     
      // Update Alert Manager Incident Settings with Category, SubCategory and Tags.
               
      console.log("Writing incident_settings");
      
          require(['jquery',"splunkjs/mvc/utils","splunkjs/mvc/savedsearchmanager"], function($, utils,SavedSearchManager) {       
                
                  // Destroy if the object is set
                  if(typeof splunkjs.mvc.Components.getInstance(incidentID) === "object") {
                     console.log("Still have object set");
                     splunkjs.mvc.Components.revokeInstance(incidentID);
                  }   
                  var incidentID="savethatsetting";  
                  console.log("incidentID:", incidentID);
               
                  // Create search manager      
                  var a_incidentID = new SavedSearchManager ({
                        "id": incidentID,
                        "searchname": "APP_CONFIG_Update_Incident_Settings",
                        "app": utils.getCurrentApp(),
                        "autostart": true
                  });
               
                  a_incidentID.on('search:error', function(properties) {
                     console.log("GotError");
                     console.log(properties);
                    
                  });
                       
                  a_incidentID.on('search:fail', function(properties) {
                    console.log("GotFail");
                       
                  });
                      
                  a_incidentID.on('search:done', function(properties) {
                     console.log("GotDone");
                     console.log("Added to incident_settings kv store");        
                  });
                               
                  console.log("GOT TO THE BOTTOM....");
            
            
          });    
           
        
          
        
      
      
}