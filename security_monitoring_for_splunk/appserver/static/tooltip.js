require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/searchmanager',
    'backbone',
    '../app/security_monitoring_for_splunk/components/ModalView',
    //'components/splunk/AlertModal',
    //    'views/shared/AlertModal.js',
    //    'views/shared/Modal.js',
    //     '/static/js/views/shared/Modal.js',
    //'../app/security_monitoring_for_splunk/components/controls/Modal',
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, TableView, SearchManager, Backbone, ModalView, Ready ) {
    
    var tokens = mvc.Components.getInstance('submitted');
    
    var assetLookup = new SearchManager({
        id: "assetSearch",
        earliest_time: "-24h@h",
        latest_time: "now",
        preview: true,
        cache: false,
        
        }, {tokens:true, tokenNamespace: "submitted"});
    
    
    // Custom Row Expansion 
    var CustomTooltipRenderer = TableView.BaseCellRenderer.extend({
        canRender: function(cell) {
            return cell.field === 'src_ip';
        },
        
        render: function($td, cell) {
            var tip;
            var src_ip = cell.value;
            // Tip only on Internal IPs we have any hope at all of finding.
            if (/^10.\d+\.\d+\.\d/.test(src_ip) || /^172.16\.\d+\.\d/.test(src_ip) || /^192.168\.\d+\.\d/.test(src_ip) ) {
                tip = "Click for Asset Info";
            } else {
                tip = "";
            }
            
            $td.html(_.template('<a href="#" id="comms-asset-lookup" data-toggle="tooltip" data-placement="right" title="<%- tip%>"><%- src_ip%></a>', {
                tip: tip,
                src_ip: src_ip 
            }));
            
            // This line wires up the Bootstrap tooltip to the cell markup
            $td.children('[data-toggle="tooltip"]').tooltip();
            
        }
    });
    
    // Asset Lookup for Modal  
    var tableclick = mvc.Components.get("comms_network_traffic_detail_table");
    tableclick.on("click:cell", function (e) {
        e.preventDefault();
        var _title = 'Asset Details for : ' + e.value;
        tokens.set('src_ip', e.value);
        var src_ip = e.value;
        assetLookup.set({ search: "| inputlookup assets.csv | search src_ip=" + src_ip + " | table src_ip nthost manager"});
        //var modal = new ModalView({ title : _title, search: assetLookup });
        //modal.show();

        
        console.log("Clicked TABLE: ", e.value, e.index, e.field, e.cell);
        
        var nt_host,manager;
        var assetResult;
        
        
        
        
        
        //assetLookup.set({ search: "| inputlookup assets.csv | search src_ip=" + src_ip + " | table src_ip nthost manager"});
        assetResult = assetLookup.data("results", { output_mode: 'json_rows', count: 0 });
        // Find any results?      
        assetLookup.on('search:done', function(properties) {
            var searchlabelID = properties.content.request.label;
            console.log("Search Label:", searchlabelID);
            if (assetLookup.attributes.data.resultCount == 0) {
                console.log("No results Found");
                return;
            }
        });
                      
        assetResult.on("data", function(properties) {
            var searchlabelID = properties.attributes.manager.id;
            console.log("Search Label:", searchlabelID, "Results:", assetResult.collection().toJSON(), "NumResults:",  assetResult.data().rows.length );
            console.log("RESULTS", assetResult.data([0]));
            
            for (i=0; i < assetResult.data().rows.length; i++ ) {
                src_ip=assetResult.data().rows[i] [0];
                nt_host = assetResult.data().rows[i] [1];
                manager = assetResult.data().rows[i] [2];    
            }
        
        // Draw Something on a screen.....
        //mvc.Components.get('modalHolder') {
        //    var html_str = "<div class=\"modal fade\" id=\"XXX\"><div class=\"modal-content\"><div class=\"modal-header\">Asset Details</div><div class=\"modal-body\"><p>SSSSSSS</p></div></div>";
            
        //}
  
        var modal = new ModalView({ title : _title, search: assetLookup, manager: manager, nt_host: nt_host, asset_ip: src_ip });
        modal.show();
                
                
               
        });       
            
    });


    mvc.Components.get('comms_network_traffic_detail_table').getVisualization(function(tableView) {
        
        // Register custom cell renderer
        tableView.table.addCellRenderer(new CustomTooltipRenderer());

        // Force the table to re-render
        tableView.table.render();
    });
    
});
