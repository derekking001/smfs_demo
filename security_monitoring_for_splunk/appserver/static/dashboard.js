require(["jquery",
            "underscore",
            "splunkjs/mvc",
            "splunkjs/mvc/textinputview",
            "splunkjs/mvc/utils",
            "/static/app/security_monitoring_for_splunk/js/Modal.js",
            "splunkjs/mvc/searchmanager",
            Splunk.util.make_full_url("/static/app/security_monitoring_for_splunk/lib/sendTelemetry.js"),  
        ],
        function($,
            _,
            mvc,
            TextInputView,
            utils,
            Modal,
            SearchManager,
            Telemetry

        ) {

          $.ajax({ 
            url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/global_config', 
            async: false, 
            success: function(returneddata) { Items = returneddata; } 

            });

			// Check if we are on the setup page already and don't redirect if we are otherwise #nastyloop
			regex = /setup/g;
			str = window.location.pathname.match(regex);

			if(Items.length == 0 && !str) {

				//e.preventDefault();
				window.location.href = "/app/security_monitoring_for_splunk/setup";

			}

            

            // Setup the record 
            let telemetryRec = {
                "url_anon": window.location.href.replace(/http:\/\/.*?\//, "http://......../").replace(/https:\/\/.*?\//, "https://......../"),
                "page": splunkjs.mvc.Components.getInstance("env").toJSON()['page'],
                "splunk_version": splunkjs.mvc.Components.getInstance("env").toJSON()['version']
                

                 };



                 // Send the record
                 try {
                     Telemetry.SendTelemetryToSplunk("PageLoad", telemetryRec);
                     
                     } 
                     catch(error) {
                     // None
                 }


        });









