function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  //queryString = decodeURIComponent(queryString);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      //paramName = paramName.toLowerCase();
      //if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }
  
  return obj;
}


// Page load stuff
// Grab URL arguments.
var analysts_comments = decodeURIComponent(getAllUrlParams().analysts_comments);
var event_source = decodeURIComponent(getAllUrlParams().event_source);
var description = decodeURIComponent(getAllUrlParams().description);
var category = decodeURIComponent(getAllUrlParams().category);
var results_analysis = decodeURIComponent(getAllUrlParams().results_analysis);
var splunk_search = decodeURIComponent(getAllUrlParams().splunk_search);
var unique_id = decodeURIComponent(getAllUrlParams().unique_id);
var fidelity = decodeURIComponent(getAllUrlParams().fidelity);
var objective = decodeURIComponent(getAllUrlParams().objective);
var pageMode = decodeURIComponent(getAllUrlParams().action);
var last_edited_by = decodeURIComponent(getAllUrlParams().last_edited_by);
var owner = decodeURIComponent(getAllUrlParams().owner);
var revNum = decodeURIComponent(getAllUrlParams().rev_num);
var mitreTactic = decodeURIComponent(getAllUrlParams().mitre_tactic);
var mitreTechnique = decodeURIComponent(getAllUrlParams().mitre_technique);
var tags = decodeURIComponent(getAllUrlParams().tags);
var name = decodeURIComponent(getAllUrlParams().name);
if(fidelity == 'High Fidelity') {
  fidelity = 'HF';
} else {
  fidelity = 'INV';
}


// Auto Fill Out Inputs if passed in from existing playbook
if(pageMode == 'display') {
  $('#analystscomments').val(analysts_comments);
  $('#eventsource').val(event_source);
  $('#description').val(description);
  $('#reportcategory').val(category);
  $('#resultsanalysis').val(results_analysis);
  $('#splunksearch').val(splunk_search);
  $('#unique_id').val(unique_id);
  $('#fidelity').val(fidelity);
  $('#objective').val(objective);
  $('#revNum').val(revNum);
  $('#lastEdited').val(last_edited_by);
  $('#owner').val(owner);
  $('#mitreTactic').val(mitreTactic);
  $('#mitreTechnique').val(mitreTechnique);
  $('#tags').val(tags);
  $('#name').val(name);
    
  var x = document.getElementById("editButton");
  x.style.display = "inline-block";
  
  //console.log("Getting Status");
 
  // Get Status of saved search
 $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/saved/searches/' + unique_id + '?f=disabled&output_mode=json',
         async: false,
         error: function(err) {
            if(err)
            {
              document.getElementById("splunkSearchStatus").style.display = "inline";
              document.getElementById("splunkSearchStatus").innerHTML = "Search Status: Search Not Yet Operational.   " + "   <i class=\"icon-x-circle\"></i> ";
            }
            },
         success: function(returneddata) {
            data = returneddata;
            if (data.entry[0].content.disabled == false){
              x = "Enabled";
              y = "icon-check";
            } else {
              x = "Disabled";
              y = "icon-x-circle";
            }
            document.getElementById("splunkSearchStatus").style.display = "inline";
            document.getElementById("splunkSearchStatus").innerHTML = "Search Status: " + x + "   <i class=\"" + y + "\"></i> ";
            }
        });
      
  
  
  } else if(pageMode == 'new') {
      var save = document.getElementById('saveButton');
      var cancel = document.getElementById('cancelButton');
      save.style.display = "inline-block";
      cancel.style.display = "inline-block";
  }
  
// If new playbook requested
if(pageMode == 'new') {
   toggleReadOnlyInputs('writeable');
  
}

// Edit Playbook
$("#editButton").click(function() {
  //console.log('EDIT CLICKED');
  var edit = document.getElementById("editButton");
  var save = document.getElementById('saveButton');
  var cancel = document.getElementById('cancelButton');
  
  //console.log('Style : ', edit.style.display);
  if (edit.style.display === "none") {
    edit.style.display = "inline-block";
  } else {
    edit.style.display = "none";
    save.style.display = "inline-block";
    cancel.style.display = "inline-block";
  }
  
  // Make inputs writeable.
  toggleReadOnlyInputs('writeable');
  document.getElementById('name').readOnly=true;
  // quick hack to stop splunkSavedSearch being available for new 'unsaved playbooks' 
  document.getElementById('splunkSavedSearch').disabled=false;
});


// Save Playbook
$("#saveButton").click(function() {
  
  document.getElementById('saveButton').style.display = 'none';
  document.getElementById('cancelButton').style.display = 'none';
  document.getElementById("editButton").style.display = 'inline-block';
  
  // Lock out controls
  toggleReadOnlyInputs('readonly');
  // quick hack to stop splunkSavedSearch being available for new 'unsaved playbooks' 
  document.getElementById('splunkSavedSearch').disabled=true;
  
  //Post Data
  var saveResult = postNewEntry();
  if(saveResult == 1) {
    $("#failure-alert").fadeTo(2000, 500).slideUp(500, function() {
       $("#failure-alert").slideUp(500);
       return;
        });
  } else {
        // Show success div/message.
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#success-alert").slideUp(500);
        });
  }
  
  setTimeout(function() {
    window.location.href = "/app/security_monitoring_for_splunk/playbook";
  },2500);
  
});

$("#runSearch").click(function() {
  console.log("Run Search Clicked");
  var searchVal = $("#splunksearch").val();
  searchVal = encodeURIComponent(searchVal);
  var url = "/app/security_monitoring_for_splunk/search?q=search%20" + searchVal;
  console.log("Trying to search: ", url);
  window.open(url, '_blank');
  
});


$("#splunkSavedSearch").click(function() {
  console.log("Save Search Clicked:");
  if(pageMode == 'new') {
    alert("Please Save the Playbook First - My Bad coding - Sorry!");
    return;
  }
  require(["jquery",
        "underscore",
        "splunkjs/mvc",
        "splunkjs/mvc/textinputview",
        "splunkjs/mvc/utils",
        "/static/app/security_monitoring_for_splunk/js/Modal.js",
        "splunkjs/mvc/searchmanager",  
    ],
    function($,
        _,
        mvc,
        TextInputView,
        utils,
        Modal,
        SearchManager
      
    ) {
    
    // Start of Save Splunk Search
    modalForSavedSearch();
    
    
    // Functions  
    function modalForSavedSearch() {
      savedSearchModal = new Modal('savedSearchModal', {
        title: 'Save New Detection Search',
        destroyOnHide: true,
        type: 'wide'
        
      });
      
      $(savedSearchModal.$el).on("hide", function() {
                // Nothing at the moment
      });
      
      $(savedSearchModal.$el).on("shown", function() {
        console.log("Shown Triggered");
        $("#savedSearchModal #searchName").val($("#unique_id").val());
        $("#savedSearchModal #search").val($("#splunksearch").val());
        $("#savedSearchModal #description").val($("#description").val());
        $("#savedSearchModal #earliest").val("-62m@m");
        $("#savedSearchModal #latest").val("-2m@m");
        $("#savedSearchModal #schedule").val("*/60 * * * *");
        
      });
      
      savedSearchModal.body
                .append($('<div class="container detect_search_container"><div class="container-fluid modalContainer" solid 1px; "><div id="searchsuccess-alert" class="alert alert-success alert-dismissable" style="background-color: #5cc05c;" hidden="true"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Success! Search Saved. </div><div id="searchfailure-alert" class="alert alert-danger alert-dismissable" style="background-color: #e66465;" hidden="true"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">times;</button>Error! Search NOT saved. </div><div class="row" style="min-width: 400px;margin-left: 10px;margin-top:40px;"><div id="aa" class="col detect_search_col1"><label for="nameofsearch" style="line-height: 20px;margin-bottom: 5px;">Search Name:</label></div><div class="col detect_search_col2"><input class="dsi" name="nameofsearch" type="text" id="searchName"></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="description">Description:</label></div><div class="col detect_search_col2"><textarea class="dsi detect_search_resize" name="description" type="text" id="description"></textarea></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="search">Search:</label></div><div class="col detect_search_col2"><textarea class="dsi detect_search_resize" name="search" type="text" id="search"></textarea></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="earliest">Earliest Time:</label></div><div class="col detect_search_col2"><input class="dsi" name="earliest" type="text" id="earliest"></div><div class="help-block detect_search_col2">Time specifiers: y, mon, d, h, m, s <a href="/en-GB/help?location=learnmore.manager.relativetime" target="_blank">Learn More <i class="icon-external"></i></a></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="latest">Latest Time:</label></div><div class="col detect_search_col2"><input class="dsi" name="latest" type="text" id="latest"></div><div class="help-block detect_search_col2">Time specifiers: y, mon, d, h, m, s <a href="/en-GB/help?location=learnmore.manager.relativetime" target="_blank">Learn More <i class="icon-external"></i></a></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="schedule">Schedule:</label></div><div class="col detect_search_col2"><input class="dsi" name="schedule" type="text" id="schedule"></div><div class="help-block detect_search_col2">e.g. 00 18 *** (every day at 6PM). <a href="/en-GB/help?location=learnmore.alert.scheduled" class="help" target="_blank" title="Splunk help" aria-label="Learn more about cron schedule">Learn More <i class="icon-external"></i></a></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="impact">Impact:</label></div><div class="col detect_search_col2"><select class="dsi" name="impact" id="impact"><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option></select></div></div><div class="row" style="margin-left: 10px;"><div class="col detect_search_col1"><label for="urgency">Urgency:</label></div><div class="col detect_search_col2"><select class="dsi" name="urgency" id="urgency"><option value="low">Low</option><option value="medium">Medium</option><option value="high">High</option></select></div></div></div>'));
     
     
      savedSearchModal.footer.append($('<button>').attr({
        type: 'button',
       'data-dismiss': 'modal'
        }).addClass('btn btn-default').text('Cancel').on('click', function() {
              savedSearchModal.hide();
              })
      );
      
      savedSearchModal.footer.append($('<button>').attr({
        type: 'button',
        
        }).addClass('btn btn-primary').text('Save').on('click', function() {
               console.log("Save Button Pressed");
                saveDetectionSearch();
                 // Get Status of saved search
               })
      );
      
      savedSearchModal.show();
          
    // end of modalForSavedSearch function  
    }
    
    
    function saveDetectionSearch() {
      //var vals = values;
      //console.log("Values: ", vals)
      
      CheckStanza = new SearchManager({
         "id": "CheckStanza",
         "cancelOnUnload": true,
         "latest_time": "",
         "status_buckets": 0,
         "earliest_time": "0",
         "app": utils.getCurrentApp(),
         "auto_cancel": 90,
         "preview": true,
         "runWhenTimeIsUndefined": false,
         "autostart": false
         }, { tokens: true, tokenNamespace: "submitted" });
      
      CheckStanza = CheckStanza.data('results', { output_mode: 'json', count: 0 });
      
      // Setup the search string
      //var uniqueSavedSearch = "9999999";
      var searchString = "| rest  /servicesNS/-/security_monitoring_for_splunk/saved/searches/ | table title description | search title=\"" + $("#savedSearchModal #searchName").val() + "\" | stats count";
      theSearch = mvc.Components.getInstance("CheckStanza");
      theSearch.set("search", searchString);
      theSearch.startSearch();
      console.log("Starting search");
      
      CheckStanza.on('search:error', function(properties) {
        console.log("Error Running Search", properties);
      });
      
      CheckStanza.on('search:fail', function(properties) {
        console.log("Search Failed to start", properties);
      });
      
      CheckStanza.on('search:done', function(properties) {
        console.log("Triggered: ", CheckStanza.attributes.data.resultCount);
        if (CheckStanza.attributes.data.resultCount == 0) {
          console.log("Search Does not exist already - creating new stanza");
          
          return;
        } else {
          console.log("Search DOES exist - Need to UPDATE this one.");
          
          return;
        }
        
      });
      
      CheckStanza.on("data", function(properties) {
        var data = CheckStanza.data().results;
        // console.log("Got my results", data)
        if (data[0].count == 0) {
             console.log("BBSearch Does not exist already - creating new stanza");
              //CreateSavedSearchConfig("mytestsearch", successCallback, failureCallback);
            
            var appscope = {};
            appscope.owner = Splunk.util.getConfigValue("USERNAME");
            appscope.app = utils.getCurrentApp();
            appscope.sharing = "app";    
             
            var svc = mvc.createService();
            var mySavedSearches = svc.savedSearches(appscope);
            
            // Create the saved search
            mySavedSearches.create({
              'search': $("#savedSearchModal #search").val(),
              'name': $("#savedSearchModal #searchName").val(),
              'description': $("#savedSearchModal #description").val(),
              'cron_schedule': $("#savedSearchModal #schedule").val(),
              'is_scheduled': true,
              'dispatch.earliest_time' : $("#savedSearchModal #earliest").val(),
              'dispatch.latest_time': $("#savedSearchModal #latest").val(),
              'actions': 'alert_manager',
              'action.alert_manager': 1,
              'action.alert_manager.param.impact': $("#savedSearchModal #impact").val(),
              'action.alert_manager.param.urgency': $("#savedSearchModal #urgency").val(),
              'action.alert_manager.param.auto_previous_resolve': 0,
              'action.alert_manager.param.auto_subsequent_resolve': 0,
              'action.alert_manager.param.auto_suppress_resolve': 0,
              'action.alert_manager.param.auto_ttl_resove': 0,
              'action.alert_manager.param.title': '$title$',
              'alert.track': 0
              
              
              }, function(errorCallback, newSearch) {
              if (errorCallback) {
                console.log("Some Error");
                
                $("#searchfailure-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#searchfailure-alert").slideUp(500);
                });
                
              } else {
              console.log("A new saved search was created");
              $("#searchsuccess-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#searchsuccess-alert").slideUp(500);
                });
              
              setTimeout(function() {
                $('#savedSearchModal').modal('hide');
              },2000);
              
              return true;
              }
            });
                 
        } else {
            console.log("BBSearch DOES exist - Need to UPDATE this one.");
             
            // Retrieve the saved search collection
            var service = mvc.createService();
            var SavedSearch = service.savedSearches();
            searchName = $("#savedSearchModal #searchName").val();
            SavedSearch.fetch(function(err, SavedSearch) {
            
              // Retrieve a specific saved search
              SavedSearch = SavedSearch.item(searchName);
            
              // Display some properties
              //console.log("Name:                " + SavedSearch.name);
              //console.log("Query:               " + SavedSearch.properties().search);
              //console.log("Description:         " + SavedSearch.properties().description);
              //console.log("Scheduled:           " + SavedSearch.properties().is_scheduled);
              //console.log("Next scheduled time: " + SavedSearch.properties().next_scheduled_time);
            
              // Modify the description and schedule the saved search
              SavedSearch.update({
                
              'search': $("#savedSearchModal #search").val(),
              'description': $("#savedSearchModal #description").val(),
              'cron_schedule': $("#savedSearchModal #schedule").val(),
              'is_scheduled': true,
              'action.alert_manager.param.impact': $("#savedSearchModal #impact").val(),
              'action.alert_manager.param.urgency': $("#savedSearchModal #urgency").val(),
              'dispatch.earliest_time' : $("#savedSearchModal #earliest").val(),
              'dispatch.latest_time': $("#savedSearchModal #latest").val()
              
              }, function() {
                console.log("\n...properties were modified...");
                $("#searchsuccess-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#searchsuccess-alert").slideUp(500);
                });
              
              setTimeout(function() {
                
                $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/saved/searches/' + searchName + '?f=disabled&output_mode=json',
         async: false,
         error: function(err) {
            if(err)
            {
              document.getElementById("splunkSearchStatus").style.display = "inline";
              document.getElementById("splunkSearchStatus").innerHTML = "Search Status: Search Not Yet Operational.   " + "   <i class=\"icon-x-circle\"></i> ";
            }
            },
         success: function(returneddata) {
            data = returneddata;
            if (data.entry[0].content.disabled == false){
              x = "Enabled";
              y = "icon-check";
            } else {
              x = "Disabled";
              y = "icon-x-circle";
            }
            document.getElementById("splunkSearchStatus").style.display = "inline";
            document.getElementById("splunkSearchStatus").innerHTML = "Search Status: " + x + "   <i class=\"" + y + "\"></i> ";
            }
        });
                
                
                $('#savedSearchModal').modal('hide');
              },2000);
              
              });
            
              // Create a small delay to allow time for the update between server and client
              splunkjs.Async.sleep(2000, function() {
            
                // Update the local copy of the object with changes
                SavedSearch.fetch(function(err, SavedSearch) {
            
                  // Display the updated properties to verify
                  //console.log("\nUpdated properties:");
                  //console.log("Description:         " + SavedSearch.properties().description);
                  //console.log("Scheduled:           " + SavedSearch.properties().is_scheduled);
                  //console.log("Next scheduled time: " + SavedSearch.properties().next_scheduled_time);
                });
              });
            });
           
        }
        
      });
      
     //endofsavedetectionsearch 
    }
    
    // End of functions
    
    // End of Require Statement
    });
  
});

// Functions

function postNewEntry() {
  
  var status = 0;
  
  var record = {
    name: $('#name').val().replace(/ /g, "_"),
    unique_id: $("#unique_id").val(),
    fidelity: $("#fidelity").val(),
    event_source: $("#eventsource").val(),
    report_category: $("#reportcategory").val(),
    description: $("#description").val(),
    objective: $("#objective").val(),
    results_analysis: $("#resultsanalysis").val(),
    splunk_search: $("#splunksearch").val(),
    analysts_comments: $("#analystscomments").val(),
    mitre_tactic: $("#mitreTactic").val(),
    mitre_technique: $("#mitreTechnique").val(),
    tags: $("#tags").val(),
    phantom: 'No',
    user: Splunk.util.getConfigValue("USERNAME")
  };

  //console.log("Record:", record);
  
  if(pageMode == 'new') {
  
    var Items = [];
      $.ajax({ url: '/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num',
             async: false, success: function(returneddata) { Items = returneddata; } });
      
      var obj = Items.find(o => o.event_source === record.event_source);
      console.log("OBJ:", obj);
      
      if(obj === undefined) {
          status = 1;
          return status;
      }
      
    var newNum = Number(obj.last_num) + 1;
    obj.last_num = newNum;
      
    $.ajax({
          url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook_unique_num/' +encodeURIComponent(obj._key),
          type: 'POST',
          contentType: "application/json",
          async: false,
          data: JSON.stringify(obj),
          success: function(returneddata) { newkey = returneddata; }
    });
  
    console.log("record name:", record.name);
    // Create unique String
    //record.unique_id = newNum + '-' + record.fidelity + '-' + record.name + '-' + record.report_category;
    record.unique_id = newNum + '-' + record.fidelity + '-' + record.name;
    // Setup Defaults. 
    record.owner = record.user;
    record.last_edited_by = record.user;
    record.rev_num = 1;
    record._time = new Date().getTime() / 1000;
  }
  
  // Editing an existing record
  if(record.rev_num != 1) {
    record.rev_num = Number($('#revNum').val()) + 1;
    record.owner = $("#owner").val();
    record.last_edited_by = record.user;
    record.date_last_edited = new Date().getTime() / 1000;
  }
  
  //console.log("Posting Record: ", record);
  
  $.ajax({
        url: '/en-US/splunkd/__raw/servicesNS/nobody/security_monitoring_for_splunk/storage/collections/data/playbook',
        type: 'POST',
        contentType: "application/json",
        async: false,
        data: JSON.stringify(record),
        success: function(returneddata) { newkey = returneddata; }
    });

}

function toggleReadOnlyInputs(action) {
  
  if(action == 'writeable') {
    document.getElementById('description').readOnly=false;
    document.getElementById('objective').readOnly=false;
    document.getElementById('resultsanalysis').readOnly=false;
    document.getElementById('splunksearch').readOnly=false;
    document.getElementById('analystscomments').readOnly=false;
    document.getElementById('mitreTactic').readOnly=false;
    document.getElementById('mitreTechnique').readOnly=false;
    document.getElementById('tags').readOnly=false;
    document.getElementById('fidelity').disabled=false;
    document.getElementById('eventsource').disabled=false;
    document.getElementById('reportcategory').disabled=false;
    document.getElementById('name').readOnly=false;
    //document.getElementById('splunkSavedSearch').disabled=false;
  } else {
    document.getElementById('description').readOnly=true;
    document.getElementById('objective').readOnly=true;
    document.getElementById('resultsanalysis').readOnly=true;
    document.getElementById('splunksearch').readOnly=true;
    document.getElementById('analystscomments').readOnly=true;
    document.getElementById('mitreTactic').readOnly=true;
    document.getElementById('mitreTechnique').readOnly=true;
    document.getElementById('tags').readOnly=true;
    document.getElementById('fidelity').disabled=true;
    document.getElementById('eventsource').disabled=true;
    document.getElementById('reportcategory').disabled=true;
    document.getElementById('name').readOnly=true;
    //document.getElementById('splunkSavedSearch').disabled=true;
  }
}






