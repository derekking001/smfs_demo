[default]
default.visualizations.custom.event-timeline-viz.event-timeline-viz.orientation = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.stack = <boolean>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tooltipDateFormat = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tooltipTimeFormat = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.zoomMax = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.zoomMin = <string>


#Tokens
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tokenStart = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tokenEnd = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tokenLabel = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tokenData = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.tokenAllVisible = <string>

#Colors
default.visualizations.custom.event-timeline-viz.event-timeline-viz.backgroundColor = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.eventColor = <string>
default.visualizations.custom.event-timeline-viz.event-timeline-viz.textColor = <string>

#New as of 1.3.0
default.visualizations.custom.event-timeline-viz.event-timeline-viz.sortGroupsBy = <string> 